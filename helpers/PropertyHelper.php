<?php

namespace app\helpers;

class PropertyHelper {

	public static function updateLowestPrice($property)
	{
		$allRooms = $property->rooms;
		$minBedPrice = null;
		foreach ($allRooms as $roomKey => $room) 
		{
			$allBeds = $room->beds;

			foreach ($allBeds as $key => $bed) 
			{
				if($minBedPrice == null)
				{
					$minBedPrice = $bed->price;
				}
				else
				{
					if($minBedPrice > $bed->price)
					{
						$minBedPrice = $bed->price;
					}
				}
			}
		}
		//updated value in object ab=nd will be available in the property object sent from the calling view
		$property->room_price_show = $minBedPrice;
	}

	public static function getDataByLocalityName($locality,$city)
	{
		$property = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
		$data = $property::find()->where(['locality_name' => $locality,'del_status' => '0','city_name' => $city])->orderBy(['id' => SORT_DESC])->limit(3)->all();
		return $data;
	}

	public static function getDataByCityName($city)
	{
		$property = new \app\modules\MubAdmin\modules\RealEstate\models\Property();
		$data = $property::find()->where(['del_status' => '0','city_name' => $city])->orderBy(['id' => SORT_DESC])->limit(3)->all();
		return $data;
	}
}
	