<?php

namespace app\migrations;
use app\commands\Migration;
/**
 * Class m171206_060009_features_support
 */
class m171206_060009_features_support extends Migration
{
    public function getTableName()
    {
        return 'feature_support_tools';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }


    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'good_till_cancelled' => "enum('available','unavailable','------------') NOT NULL DEFAULT 'available'",
            'online_demo' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'online_portfolio' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'instant_fund_withdrawal' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'relationship_managers' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'research_tips' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'brokerage_calculator' => "enum('available','unavailable') NOT NULL DEFAULT 'unavailable'",
            'margin_alculator' => "enum('available','unavailable') NOT NULL DEFAULT 'unavailable'",
            'bracket_orders' => "enum('available','unavailable') NOT NULL DEFAULT 'unavailable'",
            'cover_order' => "enum('available','unavailable') NOT NULL DEFAULT 'unavailable'",
            'training_sducation' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'in_account' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'mobile_trading' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'trading_platform' =>"enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'intraday_square' => $this->string(255),
            'free_tips' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'news_alerts' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",


            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
