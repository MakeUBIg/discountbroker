<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163533_create_post_tags extends Migration
{
    public function getTableName()
    {
        return 'post_tags';
    }
    public function getForeignKeyFields()
    {
        return [
            'tag_id' => ['mub_tag', 'id'],
            'post_id' => ['post','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'post_id' => 'post_id',
            'tag_id'  =>  'tag_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(11)->defaultValue(NULL),
            'tag_id' => $this->integer(11)->defaultValue(NULL),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
