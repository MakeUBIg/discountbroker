<?php

namespace app\migrations;
use app\commands\Migration;
/**
 * Class m171206_060037_plans
 */
class m171206_060037_plans extends Migration
{
    
   public function getTableName()
    {
        return 'plans';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }


    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'plan_1' => $this->string(255),
            'plan_2' => $this->string(255),
            'plan_3' => $this->string(255),


            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
