<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163641_create_post_comments extends Migration
{
    public function getTableName()
    {
        return 'post_comment';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_id' => ['post', 'id'],
            'mub_user_id' => ['mub_user','id'],
            'approved_by' => ['mub_user','id'],
            'comment_id' => ['post_comment','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'post_id' => 'post_id',
            'email' => 'email',
            'title' => 'title',
            'mub_user_id' => 'mub_user_id',
            'comment_id' => 'comment_id'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->defaultValue(NULL),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'comment_id' => $this->integer()->defaultValue(NULL),//if enterd comment is a reply
            'read' => "enum('0','1') NOT NULL DEFAULT '0'",
            'title' =>  $this->string()->notNull(),
            'type' => "enum('0','1') COMMENT '0 => normal, 1 => reply'",
            'comment_text' => "text",
            'mub_user_id' => $this->integer(),//can be null as well
            'status' => "enum('0','1','2','3','4') COMMENT '0 => rejected, 1 => displayed, 2 => hidden, 3 => user_blocked, 4 => not_verified' DEFAULT '4'",
            'approved_on' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'approved_by' => $this->integer(),//will be MUB USER ID
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
