<?php

namespace app\migrations;
use app\commands\Migration;


/**
 * Class m171206_060125_taxes
 */
class m171206_060125_taxes extends Migration
{ 

    
    public function getTableName()
    {
        return 'taxes';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }


    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'securities_transaction' => $this->text(),
            'exchange_transaction' => $this->string(255),
            'sebi_charges' => $this->text(),
            'goods_services' => $this->string(255),
            'stamp_duty' => $this->string(255),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
