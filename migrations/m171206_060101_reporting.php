<?php

namespace app\migrations;
use app\commands\Migration;

/**
 * Class m171206_060101_reporting
 */
class m171206_060101_reporting extends Migration
{
   
    public function getTableName()
    {
        return 'reporting';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }


    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'online_trade' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'online_pnl' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'online_contract' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'daily_market' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",

            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
