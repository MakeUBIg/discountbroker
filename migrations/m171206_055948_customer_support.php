<?php

namespace app\migrations;
use app\commands\Migration;

/**
 * Class m171206_055948_customer_support
 */
class m171206_055948_customer_support extends Migration
{
   public function getTableName()
    {
        return 'customers_support';
    }
    public function getForeignKeyFields()
    {
        return [
             'broker_id' => ['brokers', 'id'],
        ];
    }


    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'customer_service' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",      
            'email_support' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'online_live_chat' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'phone_support' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'toll_free_number' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'through_branches' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
