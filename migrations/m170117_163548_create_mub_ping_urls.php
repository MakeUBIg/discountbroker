<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163548_create_mub_ping_urls extends Migration
{
    public function getTableName()
    {
        return 'mub_ping_urls';
    }
    public function getForeignKeyFields()
    {
        return [
            'for_category' => ['mub_category','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'url' => 'url',
            'for_category' => 'for_category',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'url' => $this->integer()->defaultValue(NULL),
            'for_category' => $this->integer()->defaultValue(NULL),
            'ip' => $this->string(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
