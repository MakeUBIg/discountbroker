<?php

namespace app\migrations;
use app\commands\Migration;


/**
 * Class m171206_060019_investment
 */
class m171206_060019_investment extends Migration
{
   public function getTableName()
    {
        return 'investment';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'stock_equity' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'commodity' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'currency' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'initial_public_offers' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'mutual_funds' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'bond_ncd' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'debt' => "enum('available','unavailable') NOT NULL DEFAULT 'available'",
            'investor_category' => $this->string(255),

            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
