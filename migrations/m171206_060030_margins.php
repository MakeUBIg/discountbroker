<?php

namespace app\migrations;
use app\commands\Migration;
/**
 * Class m171206_060030_margins
 */
class m171206_060030_margins extends Migration
{
    
    public function getTableName()
    {
        return 'margins';
    }
    public function getForeignKeyFields()
    {
        return [
            'broker_id' => ['brokers', 'id'],
        ];
    }
    
    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'broker_id' => $this->integer()->notNull(),
            'm_equity_delivery' => $this->string(255),
            'm_equity_features' => $this->string(255),
            'm_equity_options' => $this->string(255),
            'm_currency_features' => $this->string(255),
            'm_currency_options' => $this->string(255),
            'm_commodity' => $this->string(255),

            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            // 'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
