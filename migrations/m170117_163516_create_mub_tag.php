<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163516_create_mub_tag extends Migration
{
    public function getTableName()
    {
        return 'mub_tag';
    }

    public function getKeyFields()
    {
        return [
            'tag_name' => 'tag_name',
            'tag_slug' => 'tag_slug'
        ];
    }

    public function getForeignKeyFields()
    {
        return [
            'category_id' => ['mub_category','id']
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $mubTag = new \app\models\MubTag();
        $mubTag->category_id = '1';
        $mubTag->tag_name = 'General';
        $mubTag->tag_slug = 'general';
        if(!$mubTag->save())
        {
            p($mubTag->getErrors());
        }
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'tag_name' => $this->string(50)->notNull(),
            'tag_slug' => $this->string(50)->notNull(),
            'category_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'", 
        ];
    }
}
