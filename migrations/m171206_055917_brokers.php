<?php

namespace app\migrations;
use app\commands\Migration;

/**
 * Class m171206_055917_brokers
 */
class m171206_055917_brokers extends Migration
{
     public function getTableName()
    {
        return 'brokers';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id'  =>  'mub_user_id',
            'broker_name' =>  'broker_name',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(11)->defaultValue(NULL),
            'broker_name' => $this->string(50),
            'incorporation_year' => $this->integer(15),
            'type_of_broker' => "enum('Full service Broker','Discount Broker','Bank Broker') NOT NULL DEFAULT 'Full service Broker'",
            'account_type' => $this->string(),
            'supported_exchanges' => $this->string(),
            'image' => $this->string(),
            'member_of' => "enum('NSDL','NSDL & CDSL','CDSL','Others') NOT NULL DEFAULT 'NSDL'",
            'broker_summary' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
