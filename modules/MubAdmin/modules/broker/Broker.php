<?php

namespace app\modules\MubAdmin\modules\broker;

class Broker extends \app\modules\MubAdmin\MubAdmin
{   
    public $controllerNamespace = 'app\modules\MubAdmin\modules\broker\controllers';
    
    public $defaultRoute = 'brokerdetail';
    
    public function init()
    {
        parent::init();
    }
}