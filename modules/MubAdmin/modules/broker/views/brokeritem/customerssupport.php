<?php

use yii\helpers\Html;?>
<div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($customerssupport, 'customer_service')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Customer Service']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($customerssupport, 'email_support')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Email Support']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($customerssupport, 'online_live_chat')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online Live Chat']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($customerssupport, 'phone_support')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Phone Support']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($customerssupport, 'toll_free_number')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Toll-free-Number']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($customerssupport, 'through_branches')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Through Branches']) ?>
        </div>
    </div>
    <div class="row">
        
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious13">Previous</a>
             <?= Html::submitButton($customerssupport->isNewRecord ? 'Create' : 'Update', ['class' => $customerssupport->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
</div>