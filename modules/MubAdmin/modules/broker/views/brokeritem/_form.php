<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->registerJsFile('/js/backend_tabs_form.js', ['depends' => [yii\web\JqueryAsset::className()]]);
// $this->registerCssFile('/css/bootstrap/bootstrap.min.css');
/* @var $this yii\web\View */
/* @var $brokers app\models\Brokers */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">Brokers</a></li>
                        <li><a href="#tab2" data-toggle="tab">Brokarege</a></li>
                        <li><a href="#tab3" data-toggle="tab">Stock Brokers Fee</a></li>
                        <li><a href="#tab4" data-toggle="tab">Taxes</a></li>
                        <li><a href="#tab5" data-toggle="tab">Transaction Charges</a></li>
                        <li><a href="#tab6" data-toggle="tab">Margins</a></li>
                        <li><a href="#tab7" data-toggle="tab">Platform</a></li>
                        <li><a href="#tab8" data-toggle="tab">Charting</a></li>
                        <li><a href="#tab9" data-toggle="tab">Brokerages Plans</a></li>
                        <li><a href="#tab10" data-toggle="tab">Reporting</a></li>
                        <li><a href="#tab11" data-toggle="tab">Features Support</a></li>
                        <li><a href="#tab12" data-toggle="tab">Investment</a></li>
                        <li><a href="#tab13" data-toggle="tab">Customer Support</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <?php $form = ActiveForm::begin(['id' => 'broker-creation'], ['options' => ['enctype' => 'multipart/form-data']]);?>
                            <div class="tab-pane fade in active" id="tab1">
                            <?= $this->render('brokers',['form' => $form,'brokers' => $brokers]);?>
                            </div>

                            <div class="tab-pane fade" id="tab2">
                            <?= $this->render('brokerage',['form' => $form,'brokerage' => $brokerage]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab3">
                            <?= $this->render('stockbrokersfee',['form' => $form,'stockbrokersfee' => $stockbrokersfee]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab4">
                            <?= $this->render('taxes',['form' => $form,'taxes' => $taxes]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab5">
                            <?= $this->render('transactioncharge',['form' => $form,'transactioncharge' => $transactioncharge]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab6">
                            <?= $this->render('margins',['form' => $form,'margins' => $margins]);?>   
                            </div>

                            <div class="tab-pane fade" id="tab7">
                            <?= $this->render('platforms',['form' => $form,'platforms' => $platforms]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab8">
                            <?= $this->render('charting',['form' => $form,'charting' => $charting]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab9">
                            <?= $this->render('plans',['form' => $form,'plans' => $plans]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab10">
                            <?= $this->render('reporting',['form' => $form,'reporting' => $reporting]);?>    
                            </div>

                            <div class="tab-pane fade" id="tab11">
                            <?= $this->render('featuresupporttools',['form' => $form,'featuresupporttools' => $featuresupporttools]);?>        
                            </div>

                            <div class="tab-pane fade" id="tab12">
                            <?= $this->render('investment',['form' => $form,'investment' => $investment]);?>      
                            </div>

                            <div class="tab-pane fade" id="tab13">
                            <?= $this->render('customerssupport',['form' => $form,'customerssupport' => $customerssupport]);?>    
                            </div>
                           
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>