<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BrokersQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brokers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brokers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
     <?= Html::a('Create Brokers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mub_user_id',
            'broker_name',
            'incorporation_year',
            'type_of_broker',
            // 'account_type',
            // 'paisa_power_classic',
            // 'supported_exchanges',
            // 'member_of',
            // 'created_at',
            // 'updated_at',
            // 'status',
            // 'del_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
