<div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($plans, 'plan_1')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($plans, 'plan_2')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($plans, 'plan_3')->textInput(['maxlength' => true]) ?>
        </div>
       
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious7">Previous</a>
            <a class="btn btn-primary" id="btnNext7">Next</a>
        </div>
</div>