 <div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($margins, 'm_equity_delivery')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($margins, 'm_equity_features')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($margins, 'm_equity_options')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($margins, 'm_currency_features')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($margins, 'm_currency_options')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($margins, 'm_commodity')->textInput() ?>
        </div>
    </div>
    <div class="row">
        
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious6">Previous</a>
            <a class="btn btn-primary" id="btnNext6">Next</a>
        </div>
</div>