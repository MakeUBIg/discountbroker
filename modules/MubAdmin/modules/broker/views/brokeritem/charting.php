<div class="brokers-form">                           
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($charting, 'intraday')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($charting, 'end_of_day')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($charting, 'coding_backtesting')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        
        </div>
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
         <a class="btn btn-primary" id="btnPrevious3">Previous</a>
         <a class="btn btn-primary" id="btnNext3">Next</a>
    </div>
 </div>