<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Brokers */

$this->title = 'Update Brokers: ' . $brokers->id;
$this->params['breadcrumbs'][] = ['label' => 'Brokers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $brokers->id, 'url' => ['view', 'id' => $brokers->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brokers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
          	'brokers' =>  $brokers,
	        'brokerage' =>  $brokerage,
	        'charting' =>  $charting,
	        'customerssupport' =>  $customerssupport,
	        'featuresupporttools' =>  $featuresupporttools,
	        'investment' =>  $investment,
	        'margins' =>  $margins,
	        'plans' =>  $plans,
	        'platforms' =>  $platforms,
	        'reporting' =>  $reporting,
	        'stockbrokersfee' =>  $stockbrokersfee,
	        'taxes' =>  $taxes,
	        'transactioncharge' =>  $transactioncharge,
    ]) ?>

</div>
