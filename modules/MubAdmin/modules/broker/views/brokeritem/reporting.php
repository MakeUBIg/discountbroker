<div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($reporting, 'online_trade')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online Trade Reports']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($reporting, 'online_pnl')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online PNL Reports']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($reporting, 'online_contract')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online Contract Notes']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($reporting, 'daily_market')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Daily Market Report']) ?>
        </div>
    </div>
    <div class="row">
        
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious9">Previous</a>
            <a class="btn btn-primary" id="btnNext9">Next</a>
        </div>
</div>