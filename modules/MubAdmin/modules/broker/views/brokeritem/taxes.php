 <div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($taxes, 'securities_transaction')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($taxes, 'exchange_transaction')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($taxes, 'sebi_charges')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($taxes, 'goods_services')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($taxes, 'stamp_duty')->textInput(['maxlength' => true]) ?>
        </div>
        
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious11">Previous</a>
            <a class="btn btn-primary" id="btnNext11">Next</a>
        </div>
</div>