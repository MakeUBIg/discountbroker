<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\ImageUploader;
/* @var $this yii\web\View */
/* @var $model app\models\Brokers */

$this->title = $brokers->id;
$this->params['breadcrumbs'][] = ['label' => 'Brokers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brokers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $brokers->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $brokers->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $brokers,
            
        

            'attributes' => [
            'broker_name',
            'incorporation_year',
            'type_of_broker',
            'account_type',
            'broker_summary',
            'supported_exchanges',
            'member_of',
        ],
    ]) ?>

</div>
