<div class="clearfix"></div>
<div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($brokerage, 'equity')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($brokerage, 'equity_futures')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($brokerage, 'equity_options')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($brokerage, 'currency_futures')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($brokerage, 'currency_options')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($brokerage, 'commodity')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($brokerage, 'other_charges')->textInput(['maxlength' => true]) ?>
        </div>
       
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious2">Previous</a>
            <a class="btn btn-primary" id="btnNext2">Next</a>

        </div>
</div>