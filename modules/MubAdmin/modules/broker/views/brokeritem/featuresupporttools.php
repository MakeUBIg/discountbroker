<div class="brokers-form">
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'good_till_cancelled')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable', '------------' => '------------'], ['prompt' => 'Select Good Till Cancelled (GTC) ']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'online_demo')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online Demo']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'online_portfolio')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Online Portfolio']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'instant_fund_withdrawal')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Instant Fund withdrawal']) ?>
                                </div>
                            </div><div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'relationship_managers')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Relationship Managers']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'research_tips')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Research & Tips']) ?>
                                </div>
                            </div><div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'brokerage_calculator')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Brokerage Calculator']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'margin_alculator')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Margin Calculator']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'bracket_orders')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Bracket orders & Trailing Stoploss']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'cover_order')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Cover order']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'training_sducation')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Training & Education']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'in_account')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select 3 in 1 Account']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'mobile_trading')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Mobile Trading']) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'trading_platform')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Trading Platform']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'intraday_square')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                <?= $form->field($featuresupporttools, 'free_tips')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Free Tips']) ?>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
                                <?= $form->field($featuresupporttools, 'news_alerts')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select News Alerts ']) ?>
                                </div>
                               
                            </div>
                            <div class="row">
                                </div><BR/ >
                            <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
                                    <a class="btn btn-primary" id="btnPrevious4">Previous</a>
                                    <a class="btn btn-primary" id="btnNext4">Next</a>
                                </div>

                            </div>