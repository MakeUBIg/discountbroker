<div class="brokers-form">
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($platforms, 'software')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Software']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($platforms, 'web_html')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Web/HTML']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
        <?= $form->field($platforms, 'mobile')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Mobile ']) ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
        <?= $form->field($platforms, 'trading_platform')->textInput() ?>
        </div>
    </div>
    <div class="row">
        
    </div>
    <div class="row">
        </div><BR/ >
    <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
            <a class="btn btn-primary" id="btnPrevious8">Previous</a>
            <a class="btn btn-primary" id="btnNext8">Next</a>
        </div>

 </div>