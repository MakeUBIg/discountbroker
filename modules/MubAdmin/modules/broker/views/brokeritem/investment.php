<div class="brokers-form">
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($investment, 'stock_equity')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Stock / Equity']) ?>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($investment, 'commodity')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Commodity']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($investment, 'currency')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Currency']) ?>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($investment, 'initial_public_offers')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Initial Public Offers (IPO)']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($investment, 'mutual_funds')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Mutual Funds']) ?>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($investment, 'bond_ncd')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Bond / NCD']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($investment, 'debt')->dropDownList([ 'available' => 'Available', 'unavailable' => 'Unavailable'], ['prompt' => 'Select Debt']) ?>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
            <?= $form->field($investment, 'investor_category')->textInput() ?>
            </div>
        </div>
        
        <div class="row">
            </div><BR/ >
        <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-1"></div><div class="form-group">
                <a class="btn btn-primary" id="btnPrevious5">Previous</a>
                <a class="btn btn-primary" id="btnNext5">Next</a>
            </div>
</div>