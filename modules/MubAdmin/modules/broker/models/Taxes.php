<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "taxes".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $securities_transaction
 * @property string $exchange_transaction
 * @property string $sebi_charges
 * @property string $goods_services
 * @property string $stamp_duty
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Taxes extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['securities_transaction', 'sebi_charges'], 'required'],
            [['broker_id'], 'integer'],
            [['securities_transaction', 'sebi_charges', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['exchange_transaction', 'goods_services', 'stamp_duty'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'securities_transaction' => 'Securities Transaction',
            'exchange_transaction' => 'Exchange Transaction',
            'sebi_charges' => 'Sebi Charges',
            'goods_services' => 'Goods Services',
            'stamp_duty' => 'Stamp Duty',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
