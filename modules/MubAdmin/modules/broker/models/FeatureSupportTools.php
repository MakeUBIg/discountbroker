<?php
namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "feature_support_tools".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $good_till_cancelled
 * @property string $online_demo
 * @property string $online_portfolio
 * @property string $instant_fund_withdrawal
 * @property string $relationship_managers
 * @property string $research_tips
 * @property string $brokerage_calculator
 * @property string $margin_alculator
 * @property string $bracket_orders
 * @property string $cover_order
 * @property string $training_sducation
 * @property string $in_account
 * @property string $mobile_trading
 * @property string $trading_platform
 * @property string $intraday_square
 * @property string $free_tips
 * @property string $news_alerts
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class FeatureSupportTools extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature_support_tools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_till_cancelled', 'online_demo', 'online_portfolio', 'instant_fund_withdrawal', 'relationship_managers', 'research_tips', 'brokerage_calculator', 'margin_alculator', 'bracket_orders', 'cover_order', 'training_sducation', 'in_account', 'mobile_trading', 'free_tips', 'news_alerts'], 'required'],
            [['broker_id'], 'integer'],
            [['good_till_cancelled', 'online_demo', 'online_portfolio', 'instant_fund_withdrawal', 'relationship_managers', 'research_tips', 'brokerage_calculator', 'margin_alculator', 'bracket_orders', 'cover_order', 'training_sducation', 'in_account', 'mobile_trading', 'free_tips', 'news_alerts', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['trading_platform', 'intraday_square'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'good_till_cancelled' => 'Good Till Cancelled',
            'online_demo' => 'Online Demo',
            'online_portfolio' => 'Online Portfolio',
            'instant_fund_withdrawal' => 'Instant Fund Withdrawal',
            'relationship_managers' => 'Relationship Managers',
            'research_tips' => 'Research Tips',
            'brokerage_calculator' => 'Brokerage Calculator',
            'margin_alculator' => 'Margin Alculator',
            'bracket_orders' => 'Bracket Orders',
            'cover_order' => 'Cover Order',
            'training_sducation' => 'Training Sducation',
            'in_account' => '3 In Account',
            'mobile_trading' => 'Mobile Trading',
            'trading_platform' => 'Trading Platform',
            'intraday_square' => 'Intraday Square',
            'free_tips' => 'Free Tips',
            'news_alerts' => 'News Alerts',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
