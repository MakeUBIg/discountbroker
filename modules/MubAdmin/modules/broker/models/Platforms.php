<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "platforms".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $software
 * @property string $web_html
 * @property string $mobile
 * @property string $trading_platform
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Platforms extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platforms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['software', 'web_html', 'mobile', 'trading_platform'], 'required'],
            [['broker_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['software', 'web_html', 'mobile', 'trading_platform'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'software' => 'Software',
            'web_html' => 'Web Html',
            'mobile' => 'Mobile',
            'trading_platform' => 'Trading Platform',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
