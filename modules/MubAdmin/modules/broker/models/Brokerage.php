<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "brokerage".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $equity
 * @property string $equity_futures
 * @property string $equity_options
 * @property string $currency_futures
 * @property string $currency_options
 * @property string $commodity
 * @property string $other_charges
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Brokerage extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokerage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equity', 'equity_futures', 'equity_options', 'currency_futures', 'currency_options', 'commodity','other_charges'], 'required'],
            [['broker_id'], 'integer'],
            [['other_charges', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['equity', 'equity_futures', 'equity_options', 'currency_futures', 'currency_options', 'commodity'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'equity' => 'Equity',
            'equity_futures' => 'Equity Futures',
            'equity_options' => 'Equity Options',
            'currency_futures' => 'Currency Futures',
            'currency_options' => 'Currency Options',
            'commodity' => 'Commodity',
            'other_charges' => 'Other Charges',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
