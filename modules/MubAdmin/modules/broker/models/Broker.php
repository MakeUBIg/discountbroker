<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "brokers".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $broker_name
 * @property integer $incorporation_year
 * @property string $type_of_broker
 * @property string $account_type
 * @property string $supported_exchanges
 * @property string $image
 * @property string $member_of
 * @property string $broker_summary
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokerage[] $brokerages
 * @property User $mubUser
 * @property Charting[] $chartings
 * @property CustomersSupport[] $customersSupports
 * @property FeatureSupportTools[] $featureSupportTools
 * @property Investment[] $investments
 * @property Margins[] $margins
 * @property Plans[] $plans
 * @property Platforms[] $platforms
 * @property Reporting[] $reportings
 * @property StockBrokersFee[] $stockBrokersFees
 * @property Taxes[] $taxes
 * @property TransactionCharge[] $transactionCharges
 */
class Broker extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'incorporation_year'], 'integer'],
            [['type_of_broker', 'member_of', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['broker_name'], 'string', 'max' => 50],
            [['account_type', 'supported_exchanges', 'image', 'broker_summary'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'broker_name' => 'Broker Name',
            'incorporation_year' => 'Incorporation Year',
            'type_of_broker' => 'Type Of Broker',
            'account_type' => 'Account Type',
            'supported_exchanges' => 'Supported Exchanges',
            'image' => 'Image',
            'member_of' => 'Member Of',
            'broker_summary' => 'Broker Summary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokerages()
    {
        return $this->hasMany(Brokerage::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(User::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChartings()
    {
        return $this->hasMany(Charting::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomersSupports()
    {
        return $this->hasMany(CustomersSupport::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureSupportTools()
    {
        return $this->hasMany(FeatureSupportTools::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestments()
    {
        return $this->hasMany(Investment::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMargins()
    {
        return $this->hasMany(Margins::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatforms()
    {
        return $this->hasMany(Platforms::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportings()
    {
        return $this->hasMany(Reporting::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockBrokersFees()
    {
        return $this->hasMany(StockBrokersFee::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes()
    {
        return $this->hasMany(Taxes::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionCharges()
    {
        return $this->hasMany(TransactionCharge::className(), ['broker_id' => 'id']);
    }
}
