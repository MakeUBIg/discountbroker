<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "customers_support".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $customer_service
 * @property string $email_support
 * @property string $online_live_chat
 * @property string $phone_support
 * @property string $toll_free_number
 * @property string $through_branches
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class CustomersSupport extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers_support';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_service',  'email_support', 'online_live_chat', 'phone_support', 'toll_free_number', 'through_branches'], 'required'],
            [['broker_id'], 'integer'],
            [['customer_service', 'email_support', 'online_live_chat', 'phone_support', 'toll_free_number', 'through_branches', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'customer_service' => 'Customer Service',
            'email_support' => 'Email Support',
            'online_live_chat' => 'Online Live Chat',
            'phone_support' => 'Phone Support',
            'toll_free_number' => 'Toll Free Number',
            'through_branches' => 'Through Branches',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
