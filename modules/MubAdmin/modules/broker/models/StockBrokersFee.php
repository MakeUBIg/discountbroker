<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "stock_brokers_fee".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property integer $trading_only
 * @property integer $trading_Demat
 * @property integer $demat_account_amc
 * @property integer $commodity
 * @property string $dp_transaction
 * @property integer $offline_order_placing
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class StockBrokersFee extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_brokers_fee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['trading_only', 'trading_Demat', 'demat_account_amc', 'commodity', 'offline_order_placing'], 'required'],
            [['broker_id'], 'integer'],
            [['dp_transaction', 'del_status', 'trading_only', 'trading_Demat', 'demat_account_amc', 'commodity', 'offline_order_placing'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'trading_only' => 'Trading Only',
            'trading_Demat' => 'Trading  Demat',
            'demat_account_amc' => 'Demat Account Amc',
            'commodity' => 'Commodity',
            'dp_transaction' => 'Dp Transaction',
            'offline_order_placing' => 'Offline Order Placing',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
