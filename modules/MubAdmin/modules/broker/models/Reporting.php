<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "reporting".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $online_trade
 * @property string $online_pnl
 * @property string $online_contract
 * @property string $daily_market
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Reporting extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['online_trade', 'online_pnl', 'online_contract', 'daily_market'], 'required'],
            [['broker_id'], 'integer'],
            [['online_trade', 'online_pnl', 'online_contract', 'daily_market', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'online_trade' => 'Online Trade',
            'online_pnl' => 'Online Pnl',
            'online_contract' => 'Online Contract',
            'daily_market' => 'Daily Market',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
