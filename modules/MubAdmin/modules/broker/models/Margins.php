<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "margins".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $m_equity_delivery
 * @property string $m_equity_features
 * @property string $m_equity_options
 * @property string $m_currency_features
 * @property string $m_currency_options
 * @property string $m_commodity
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Margins extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'margins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_equity_delivery', 'm_equity_features', 'm_equity_options', 'm_currency_features', 'm_currency_options', 'm_commodity'], 'required'],
            [['broker_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['m_equity_delivery', 'm_equity_features', 'm_equity_options', 'm_currency_features', 'm_currency_options', 'm_commodity'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'm_equity_delivery' => 'M Equity Delivery',
            'm_equity_features' => 'M Equity Features',
            'm_equity_options' => 'M Equity Options',
            'm_currency_features' => 'M Currency Features',
            'm_currency_options' => 'M Currency Options',
            'm_commodity' => 'M Commodity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
