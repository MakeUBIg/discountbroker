<?php

namespace app\modules\MubAdmin\modules\broker\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Brokers;

/**
 * BrokerSearch represents the model behind the search form about `app\models\Brokers`.
 */
class BrokerSearch extends Brokers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'incorporation_year'], 'integer'],
            [['broker_name', 'type_of_broker', 'account_type', 'supported_exchanges', 'member_of', 'created_at', 'updated_at', 'status', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brokers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'incorporation_year' => $this->incorporation_year,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'broker_name', $this->broker_name])
            ->andFilterWhere(['like', 'type_of_broker', $this->type_of_broker])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'supported_exchanges', $this->supported_exchanges])
            ->andFilterWhere(['like', 'member_of', $this->member_of])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
