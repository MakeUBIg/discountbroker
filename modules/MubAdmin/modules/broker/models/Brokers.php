<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;
use app\models\MubUser;
use app\models\User;

/**
 * This is the model class for table "brokers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $broker_name
 * @property integer $incorporation_year
 * @property string $type_of_broker
 * @property string $account_type
 * @property string $paisa_power_classic
 * @property string $supported_exchanges
 * @property string $member_of
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokerage[] $brokerages
 * @property User $user
 * @property Charting[] $chartings
 * @property CustomersSupport[] $customersSupports
 * @property FeatureSupportTools[] $featureSupportTools
 * @property Investment[] $investments
 * @property Margins[] $margins
 * @property Plans[] $plans
 * @property Platforms[] $platforms
 * @property Reporting[] $reportings
 * @property StockBrokersFee[] $stockBrokersFees
 * @property Taxes[] $taxes
 * @property TransactionCharge[] $transactionCharges
 */
class Brokers extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brokers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'incorporation_year'], 'integer'],
            [['broker_name', 'incorporation_year', 'type_of_broker', 'account_type', 'member_of', 'supported_exchanges',], 'required'],
            [['type_of_broker', 'account_type', 'member_of','broker_summary', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at','image'], 'safe'],
            [['image'], 'file','extensions'=>'jpg, png'],
            [['broker_name'], 'string', 'max' => 50],
            [['supported_exchanges'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'User ID',
            'broker_name' => 'Broker Name',
            'incorporation_year' => 'Incorporation Year',
            'type_of_broker' => 'Type Of Broker',
            'account_type' => 'Account Type',
            'supported_exchanges' => 'Supported Exchanges',
            'image' => 'Image',
            'member_of' => 'Member Of',
            'broker_summary' => 'Broker Summary',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrokerages()
    {
        return $this->hasOne(Brokerage::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(User::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChartings()
    {
        return $this->hasOne(Charting::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomersSupports()
    {
        return $this->hasOne(CustomersSupport::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureSupportTools()
    {
        return $this->hasOne(FeatureSupportTools::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestments()
    {
        return $this->hasOne(Investment::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMargins()
    {
        return $this->hasOne(Margins::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasOne(Plans::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatforms()
    {
        return $this->hasOne(Platforms::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportings()
    {
        return $this->hasOne(Reporting::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockBrokersFees()
    {
        return $this->hasOne(StockBrokersFee::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxes()
    {
        return $this->hasOne(Taxes::className(), ['broker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionCharges()
    {
        return $this->hasOne(TransactionCharge::className(), ['broker_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BrokersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrokersQuery(get_called_class());
    }
}
