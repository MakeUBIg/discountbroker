<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "investment".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $stock_equity
 * @property string $commodity
 * @property string $currency
 * @property string $initial_public_offers
 * @property string $mutual_funds
 * @property string $bond_ncd
 * @property string $debt
 * @property string $investor_category
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class Investment extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['stock_equity', 'commodity', 'currency', 'initial_public_offers', 'mutual_funds', 'bond_ncd', 'debt'], 'required'],
            [['broker_id'], 'integer'],
            [['stock_equity', 'commodity', 'currency', 'initial_public_offers', 'mutual_funds', 'bond_ncd', 'debt', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['investor_category'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'stock_equity' => 'Stock Equity',
            'commodity' => 'Commodity',
            'currency' => 'Currency',
            'initial_public_offers' => 'Initial Public Offers',
            'mutual_funds' => 'Mutual Funds',
            'bond_ncd' => 'Bond Ncd',
            'debt' => 'Debt',
            'investor_category' => 'Investor Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
