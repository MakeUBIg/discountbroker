<?php 

namespace app\modules\MubAdmin\modules\broker\models;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use yii\helpers\ArrayHelper;
use Yii;

class BrokerProcess extends Model
{
    public $model = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
     $brokers = new Brokers();
     $brokerage = new Brokerage();
     $charting = new Charting();
     $customerssupport = new CustomersSupport();
     $featuresupporttools = new FeatureSupportTools();
     $investment = new Investment();
     $margins = new Margins();
     $plans = new Plans();
     $platforms = new Platforms();
     $reporting = new Reporting();
     $stockbrokersfee = new StockBrokersFee();
     $taxes = new Taxes();
     $transactioncharge = new TransactionCharge();

     $this->model = [
            'brokers' =>  $brokers,
            'brokerage' =>  $brokerage,
            'charting' =>  $charting,
            'customerssupport' =>  $customerssupport,
            'featuresupporttools' =>  $featuresupporttools,
            'investment' =>  $investment,
            'margins' =>  $margins,
            'plans' =>  $plans,
            'platforms' =>  $platforms,
            'reporting' =>  $reporting,
            'stockbrokersfee' =>  $stockbrokersfee,
            'taxes' =>  $taxes,
            'transactioncharge' =>  $transactioncharge,  
        ];
        return $this->model;
    }

    public function getComparedBrokers($params)
    {
        $allBrokers = [];
        if(!empty($params['broker_first'])){
            $allBrokers['first'] = $this->getBrokerDetails($params['broker_first']);
        }
        if(!empty($params['broker_second'])) {
            $allBrokers['second'] = $this->getBrokerDetails($params['broker_second']);
        }
        if(!empty($params['broker_third'])){
            $allBrokers['third'] = $this->getBrokerDetails($params['broker_third']);
        }
        // return ArrayHelper::toArray($allBrokers);
        return $allBrokers;
    }

    public function getBrokerDetails($id)
    {  
        $brokerModel = new Brokers();
        $broker = $brokerModel->find()->where(['id' => $id,'del_status' => '0'])->one();
        //p($broker->brokerages);
        $storeBrokerages = $broker->brokerages;
        $storePlans = $broker->plans;
        $storecharting = $broker->chartings;
        $customerssupports = $broker->customersSupports;
        $storefeaturesupporttools = $broker->featureSupportTools;
        $storeinvestment = $broker->investments;
        $storemargins = $broker->margins;
        $storeplatforms = $broker->platforms;
        $storereporting = $broker->reportings;
        $storestockbrokersfee = $broker->stockBrokersFees;
        $storetaxes = $broker->taxes;
        $storetransactioncharge = $broker->transactionCharges;
        $storeTable = array('broker' => $broker,'brokerages' => $storeBrokerages,'plans'=>$storePlans,'chartings'=>$storecharting,'customersSupports'=>$customerssupports,'featureSupportTools'=>$storefeaturesupporttools,'investments'=>$storeinvestment,'margins'=>$storemargins,'platforms'=>$storeplatforms,'reportings'=>$storereporting,'stockBrokersFees'=>$storestockbrokersfee,'taxes'=>$storetaxes,'transactionCharges'=>$storetransactioncharge);
        // p(ArrayHelper::toArray($storeTable));
        return $storeTable;
    }

    public function getFormData()
    {
        return [];
    }
    
    public function getRelatedModels($model)
    {
        $brokerage = $model->brokerages;
        $charting = $model->chartings;
        $customersSupport = $model->customersSupports;
        $featureSupportTools = $model->featureSupportTools;
        $investment = $model->investments;
        $margins = $model->margins;
        $plans = $model->plans;
        $platforms = $model->platforms;
        $reporting = $model->reportings;
        $stockBrokersFee = $model->stockBrokersFees;
        $taxes = $model->taxes;
        $transactionCharge = $model->transactionCharges;

      $this->relatedModels = [
            'brokers' => $model,
            'brokerage' => $brokerage,
            'charting' => $charting,
            'customerssupport' => $customersSupport,
            'featuresupporttools' => $featureSupportTools,
            'investment' => $investment,
            'margins' => $margins,
            'plans' => $plans,
            'platforms' => $platforms,
            'reporting' => $reporting,
            'stockbrokersfee' => $stockBrokersFee,
            'taxes' => $taxes,
            'transactioncharge' => $transactionCharge,
        ];
        return $this->relatedModels;   
    }

    public function saveBrokers($brokers)
    {   
        $image = UploadedFile::getInstance($brokers,'image');
        if($image)
        {
            $this->SaveBrokerImage($brokers);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $brokersModel = new Brokers();
                $image = $brokersModel->findOne($brokers->id)->image;
                $brokers->image = $image;
            }
        }
        $userId = \app\models\User::getMubUserId();
        $brokers->mub_user_id =  $userId;
        return ($brokers->save()) ? $brokers->id : p($brokers->getErrors());
    }

    public function saveBrokerImage($broker)
    {
        $broker->image = UploadedFile::getInstance($broker,'image');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($broker,'image');
        return $broker;
    }

    public function saveBrokerage($brokerage,$brokerId)
    {        
        $brokerage->broker_id = $brokerId;
        return ($brokerage->save()) ? $brokerage->id : p($brokerage->getErrors());
    }

    public function saveCharting($charting,$brokerId)
    {        
        $charting->broker_id = $brokerId;
        return ($charting->save()) ? $charting->id : p($charting->getErrors());
    }

    public function saveCustomersSupport($customerssupport,$brokerId)
    {        
        $customerssupport->broker_id = $brokerId;
        return ($customerssupport->save()) ? $customerssupport->id : p($customerssupport->getErrors());
    }

    public function saveFeatureSupportTools($featuresupporttools,$brokerId)
    {        
        $featuresupporttools->broker_id = $brokerId;
        return ($featuresupporttools->save()) ? $featuresupporttools->id : p($featuresupporttools->getErrors());
    }

    public function saveInvestment($investment,$brokerId)
    {        
        $investment->broker_id = $brokerId;
        return ($investment->save()) ? $investment->id : p($investment->getErrors());
    }

    public function saveMargins($margins,$brokerId)
    {        
        $margins->broker_id = $brokerId;
        return ($margins->save()) ? $margins->id : p($margins->getErrors());
    }

    public function savePlans($plans,$brokerId)
    {        
        $plans->broker_id = $brokerId;
        return ($plans->save()) ? $plans->id : p($plans->getErrors());
    }
   
    public function savePlatforms($platforms,$brokerId)
    {        
        $platforms->broker_id = $brokerId;
        return ($platforms->save()) ? $platforms->id : p($platforms->getErrors());
    }

    public function saveReporting($reporting,$brokerId)
    {        
        $reporting->broker_id = $brokerId;
        return ($reporting->save()) ? $reporting->id : p($reporting->getErrors());
    }

    public function saveStockBrokersFee($stockbrokersfee,$brokerId)
    {        
        $stockbrokersfee->broker_id = $brokerId;
        return ($stockbrokersfee->save()) ? $stockbrokersfee->id : p($stockbrokersfee->getErrors());
    }

    public function saveTaxes($taxes,$brokerId)
    {        
        $taxes->broker_id = $brokerId;
        return ($taxes->save()) ? $taxes->id : p($taxes->getErrors());
    }

    public function saveTransactionCharge($transactioncharge,$brokerId)
    {        
        $transactioncharge->broker_id = $brokerId;
        return ($transactioncharge->save()) ? $transactioncharge->id : p($transactioncharge->getErrors());
    }

    public function saveData($data = [])
    {
     if (isset($data['brokers']))
        {
        try {
               $brokerId = $this->saveBrokers($data['brokers']);
               if($brokerId)
                {
                    $brokerage = $this->saveBrokerage($data['brokerage'],$brokerId);
                    if($brokerage)
                    {
                        $charting = $this->savecharting($data['charting'],$brokerId);
                        if($charting)
                        {
                            $customerssupport = $this->savecustomerssupport($data['customerssupport'],$brokerId);
                            if($customerssupport)
                            {
                                $featuresupporttools = $this->savefeaturesupporttools($data['featuresupporttools'],$brokerId);
                                if($featuresupporttools)
                                {
                                    $investment = $this->saveinvestment($data['investment'],$brokerId);
                                    if($investment)
                                    {
                                        $margins = $this->savemargins($data['margins'],$brokerId);
                                        if($margins)
                                        {
                                            $plans = $this->saveplans($data['plans'],$brokerId);
                                            if($plans)
                                            {
                                                $platforms = $this->saveplatforms($data['platforms'],$brokerId);
                                                if($platforms)
                                                {
                                                    $reporting = $this->savereporting($data['reporting'],$brokerId);
                                                    if($reporting)
                                                    {
                                                        $stockbrokersfee = $this->savestockbrokersfee($data['stockbrokersfee'],$brokerId);
                                                        if($stockbrokersfee)
                                                        {
                                                            $taxes = $this->savetaxes($data['taxes'],$brokerId);
                                                            if($taxes)
                                                            {
                                                                $transactioncharge = $this->savetransactioncharge($data['transactioncharge'],$brokerId);
                                                                if($transactioncharge)
                                                                {
                                                                    return $brokerId;
                                                                }
                                                                p($data['transactioncharge']->getErrors());
                                                            }
                                                            p($data['taxes']->getErrors());
                                                        }
                                                        p($data['stockbrokersfee']->getErrors());
                                                    }
                                                    p($data['reporting']->getErrors());
                                                }
                                                p($data['platforms']->getErrors());
                                            }
                                            p($data['plans']->getErrors());
                                        }
                                        p($data['margins']->getErrors());
                                    }
                                    p($data['investment']->getErrors());
                                }
                                p($data['featuresupporttools']->getErrors());
                            }
                            p($data['customerssupport']->getErrors());
                        }
                        p($data['charting']->getErrors());
                    }
                    p($data['brokerage']->getErrors());
                }
                p(['bottom' => $data['brokers']->getErrors()]);
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        } 
        else
        {
            throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
        }
    }
}