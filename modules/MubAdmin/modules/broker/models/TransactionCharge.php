<?php

namespace app\modules\MubAdmin\modules\broker\models;

use Yii;

/**
 * This is the model class for table "transaction_charge".
 *
 * @property integer $id
 * @property integer $broker_id
 * @property string $equity_delivery
 * @property string $equity_intraday
 * @property string $equity_futures
 * @property string $equity_options
 * @property string $currency_futures
 * @property string $currency_options
 * @property string $commodity
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property Brokers $broker
 */
class TransactionCharge extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_charge';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equity_delivery', 'equity_intraday', 'equity_futures', 'equity_options', 'currency_futures', 'currency_options', 'commodity'], 'required'],
            [['broker_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['equity_delivery', 'equity_intraday', 'equity_futures', 'equity_options', 'currency_futures', 'currency_options', 'commodity'], 'string', 'max' => 255],
            [['broker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brokers::className(), 'targetAttribute' => ['broker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'broker_id' => 'Broker ID',
            'equity_delivery' => 'Equity Delivery',
            'equity_intraday' => 'Equity Intraday',
            'equity_futures' => 'Equity Futures',
            'equity_options' => 'Equity Options',
            'currency_futures' => 'Currency Futures',
            'currency_options' => 'Currency Options',
            'commodity' => 'Commodity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBroker()
    {
        return $this->hasOne(Brokers::className(), ['id' => 'broker_id']);
    }
}
