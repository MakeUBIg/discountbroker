<?php

namespace app\modules\MubAdmin\modules\broker\models;
use app\components\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Brokers]].
 *
 * @see Brokers
 */
class BrokersQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Brokers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Brokers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
