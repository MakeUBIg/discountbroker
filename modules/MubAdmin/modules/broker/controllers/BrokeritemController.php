<?php

namespace app\modules\MubAdmin\modules\broker\controllers;

use Yii;
use app\modules\MubAdmin\modules\broker\models\BrokerProcess;
use app\modules\MubAdmin\modules\broker\models\Brokers;
use app\modules\MubAdmin\modules\broker\models\BrokerSearch;
use app\modules\MubAdmin\modules\broker\models\Brokerage;
use app\modules\MubAdmin\modules\broker\models\BrokersQuery;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class BrokeritemController extends MubController
{
    public function getProcessModel()
    {
        return new BrokerProcess();
    }

    public function getPrimaryModel()
    {
        return new Brokers();
    }

    public function getSearchModel()
    {
        return new BrokerSearch();
    }
}