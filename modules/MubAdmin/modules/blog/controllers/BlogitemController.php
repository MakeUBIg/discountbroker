<?php

namespace app\modules\MubAdmin\modules\blog\controllers;

use Yii;
use app\models\Post;
use app\models\PostSearch;
use app\components\MubController;
use app\modules\MubAdmin\modules\blog\models\BlogProcess;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class BlogitemController extends MubController
{
   public function getPrimaryModel()
   {
        return new Post();
   }

   public function getProcessModel()
   {
        return new BlogProcess();
   }

   public function getSearchModel()
   {
        return new PostSearch();
   }
}
