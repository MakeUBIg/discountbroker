<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $mubCategory app\mubCategorys\MubCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mub-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($mubCategory, 'id') ?>

    <?= $form->field($mubCategory, 'category_name') ?>

    <?= $form->field($mubCategory, 'category_slug') ?>

    <?= $form->field($mubCategory, 'category_parent') ?>

    <?= $form->field($mubCategory, 'category_child') ?>

    <?php // echo $form->field($mubCategory, 'created_at') ?>

    <?php // echo $form->field($mubCategory, 'updated_at') ?>

    <?php // echo $form->field($mubCategory, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>