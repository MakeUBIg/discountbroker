<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $mubCategory app\mubCategorys\MubCategory */

$this->title = Yii::t('app', 'Add a Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Added Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'mubCategory' => $mubCategory,
    ]) ?>

</div>