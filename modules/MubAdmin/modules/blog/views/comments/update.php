<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $postComment app\postComments\PostComment */

	$user = new \app\models\MubUser();
	$currentUser = $user::find()->where(['user_id' => \Yii::$app->user->id])->one();
	$userContact = $currentUser->mubUserContacts;

$this->title = Yii::t('app', 'Update {postCommentClass}: ', [
    'postCommentClass' => 'A Comment',
]) . $postComment->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Post Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $postComment->name, 'url' => ['view', 'id' => $postComment->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="post-comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'postComment' => $postComment,
        'posts' => $posts,
        'user' => $currentUser,
        'userContact' => $userContact
    ]) ?>

</div>
