<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $post app\models\Post */

$this->title = Yii::t('app', 'Create Articles');
?>
<div class="post-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'post' => $post,
        'postDetail' => $postDetail,
        'postImages' => $postImages,
        'postCategory' => $postCategory,
        'allCategories' => $allCategories
    ]) ?>

</div>
