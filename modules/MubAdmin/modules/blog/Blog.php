<?php

namespace app\modules\MubAdmin\modules\blog;

class Blog extends \app\modules\MubAdmin\MubAdmin
{
    public $controllerNamespace = 'app\modules\MubAdmin\modules\blog\controllers';
    
    public $defaultRoute = 'blogitem';
    public function init()
    {
        parent::init();
    }
}
