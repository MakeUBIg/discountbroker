<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\RealEstate\models\AmenitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Amenities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
<div class="amenity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Amenity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'icon_url',
            'amenity_category',
            'amenity_name',

            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'active';
                    $id = $model->id;
                    $valInactive = 'inactive';
                    return ($model->status == 'inactive') ? '<button onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valActive.'\''.','.'\''.$id.'\''.')">Activate</button>' : '<button onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valInactive.'\''.','.'\''.$id.'\''.')">Deactivate</button>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                },
            ],
            // 'amenity_price',
            // 'created_at',
            // 'updated_at',
            // 'del_status',
             ['header' => 'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div></div>
