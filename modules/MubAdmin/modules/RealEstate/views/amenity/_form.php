<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $amenity app\modules\MubAdmin\modules\RealEstate\amenitys\Amenity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amenity-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($amenity, 'icon_url')->textInput(['maxlength' => true])->label('Icon Code');?>    

    <?= $form->field($amenity, 'amenity_category')->dropDownList([ 'Basic' => 'Basic', 'Special' => 'Special', ], ['prompt' => 'Select Amenity Type']);?>

    <?= $form->field($amenity, 'amenity_name')->textInput(['maxlength' => true]);?>
    <?php  $userRole = \Yii::$app->controller->getUserRole();
        if($userRole == 'admin' || $userRole == 'superadmin'){?>

    <?= $form->field($amenity, 'status')->dropDownList(['active' => 'Active','inactive' => 'InActive'], ['prompt' => 'Select Amenity Status']);?>
        
        <?php }?>

   <div class="form-group">
   <div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($amenity->isNewRecord ? 'Create' : 'Update', ['class' => $amenity->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['/mub-admin/real-estate/amenity'], ['class' => 'btn btn-warning cancel'])?>
        </div>
    </div>
</div></div>
    <?php ActiveForm::end(); ?>

</div>
