<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $amenity app\modules\MubAdmin\modules\RealEstate\amenitys\Amenity */

$this->title = 'Create Amenity';
$this->params['breadcrumbs'][] = ['label' => 'Amenities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amenity-create">
<div class="col-md-10 col-md-offset-1" >

    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'amenity' => $amenity,
    ]) ?>

</div>
