<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $amenity app\modules\MubAdmin\modules\RealEstate\amenitys\Amenity */

$this->title = 'Update Amenity: ' . $amenity->id;
$this->params['breadcrumbs'][] = ['label' => 'Amenities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $amenity->id, 'url' => ['view', 'id' => $amenity->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="amenity-update">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'amenity' => $amenity,
    ]) ?>
</div>
