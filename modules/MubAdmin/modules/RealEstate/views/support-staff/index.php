<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Property Support Staff';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-support-staff-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Property Support Staff', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            
            'staff_name',
            'email:email',
             'mobile',
            // 'state_id',
             'city_name',
            // 'salary',
             'designation',
            // 'experience',
             'visite_charge',
             'type',
            // 'joining_date',
            // 'left_date',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['header' => 'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
