<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff */

$this->title = 'Create Property Support Staff';
$this->params['breadcrumbs'][] = ['label' => 'Property Support Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-support-staff-create">
	<div class="col-md-10 col-md-offset-1" >
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'supportStaff' => $supportStaff,
        'activeProperties' => $activeProperties,
        'allStates' => $allStates
    ]) ?>

</div>
