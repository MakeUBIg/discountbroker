<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff */

$this->title = $supportStaff->staff_name;
$this->params['breadcrumbs'][] = ['label' => 'Property Support Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-support-staff-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('All Support Staff', ['/mub-admin/real-estate/support-staff'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Update', ['update', 'id' => $supportStaff->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $supportStaff->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $supportStaff,
        'attributes' => [
            'id',
            'property_id',
            'staff_name',
            'email:email',
            'mobile',
            'salary',
            'city_name',
            'designation',
            'experience',
            'visite_charge',
            'type',
            'joining_date',
            'left_date'
        ],
    ]) ?>

</div>
