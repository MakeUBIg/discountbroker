<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff;
/* @var $this yii\web\View */
/* @var $supportStaff app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-support-staff-form">

    <?php $form = ActiveForm::begin();
     ?>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'property_id')->dropDownList($activeProperties, ['prompt' => 'Select A Property'])->label('Property Name') ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($supportStaff, 'staff_name')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'email')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($supportStaff, 'mobile')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'state_id')->dropDownList($allStates, ['id' => 'mub-state','prompt' => 'Select A state']);?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($supportStaff, 'city_name')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'salary')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($supportStaff, 'designation')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'experience')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($supportStaff, 'visite_charge')->textInput() ?>
</div>
<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff, 'type')->dropDownList([ 'regular' => 'Regular', 'professional' => 'Professional', ], ['prompt' => 'Select Your Type']) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($supportStaff,'joining_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options' => ['class'=> 'form-control']]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
   <?= $form->field($supportStaff,'left_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options' => ['class'=> 'form-control']]) ?>
</div> 
    
 <div class="form-group">
    <div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($supportStaff->isNewRecord ? 'Create' : 'Update', ['class' => $supportStaff->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div></div>

    <?php ActiveForm::end(); ?>

</div>
