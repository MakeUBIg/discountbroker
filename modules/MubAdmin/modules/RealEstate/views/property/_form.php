<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\fileupload\FileUploadUI;
use app\helpers\ImageUploader;
use dosamigos\tinymce\TinyMce;

$stateId = \Yii::$app->request->getQueryParam('state');
        if($stateId == '')
        {
            $stateId = \Yii::$app->request->getBodyParam('state');
        }
$propertyImages = new app\modules\MubAdmin\modules\RealEstate\models\PropertyImages();
$images = $propertyImages::find()->where(['property_id' => $property->id,'del_status' => '0'])->all();

/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\RealEstate\propertys\Property */
/* @var $form yii\widgets\ActiveForm */

$state = new \app\models\State();

$stateName = \app\helpers\StringHelper::generateSlug($state::findOne($stateId)->state_name);
?>
<?php if(!empty($allAmenities)){?>
<style>
    .toggle
    {
        display:none;
    }
</style>

<div class="property-form">

    <?php $form = ActiveForm::begin(); ?>
<?php if(\Yii::$app->controller->action->id == 'update'){?>
 <?= FileUploadUI::widget([
    'model' => $propertyImages,
    'attribute' => 'url',
    'url' => ['property/property-image', 'state' => \Yii::$app->request->getQueryParam('state'),'property' => $property->id],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 5000000,
        'load' => true
    ],
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]); ?>

<?php if(!empty($images)){?>
 <?php  foreach($images as $key=>$image){?>
    <div id="uploaded<?php echo $key;?>" style="display: inline; margin-right: 10px;">
    <?php echo Html::img(ImageUploader::resizeRender($image->url, '100', '100'),['height'=>100,'width'=>100]);
    echo Html::a(
        Yii::t('app', 'x'),
        '#',
        ["class" => "btn btn-danger del-image","id" => $image->title]);
    ?>
     </div>
     <?php }}}?>
     <div class="row">
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($property, 'property_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($property, 'property_type')->dropDownList([ 'hostel' => 'Hostel', 'pg' => 'Pg', 'flat' => 'Flat','house' => 'House' ], ['prompt' => 'Select Property Type']) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($property, 'property_area')->dropDownList([ 'boys' => 'Boys', 'girls' => 'Girls', 'both' => 'Both', 'family' => 'Family', ], ['prompt' => 'Select Property For'])->label('Property For') ?>
    </div>
    <?= $form->field($property,'lat')->hiddenInput(['id' => 'lat'])->label(false);?>
    
    <?= $form->field($property,'long')->hiddenInput(['id' => 'long'])->label(false);?>
    
    <?= $form->field($property,'state_name')->hiddenInput(['id' => 'state_name'])->label(false);?>
    
    <?= $form->field($property,'city_name')->hiddenInput(['id' => 'city_name'])->label(false);?>
    
    <?= $form->field($property,'pincode')->hiddenInput(['id' => 'pincode'])->label(false);?>
    
    <?= $form->field($property,'locality_name')->hiddenInput(['id' => 'locality'])->label(false);?>
   
   <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($property, 'property_category')->dropDownList(['economy' => 'Economy Property','flagship' => 'FlagShip Property','premium' => 'Premium Property'], ['prompt' => 'Select Property Category'])?>
    </div>
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($property, 'sa_a')->textInput(['maxlength' => true,'placeholder' => 'Like plot Number/Khasra number'])->label('Street address Line1');?>
    </div>
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 ">
    <?= $form->field($property, 'sa_b')->textInput(['maxlength' => true,'placeholder'=> 'Sector / Locality name'])->label('Street address Line2'); ?>
    </div>
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($property, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'en_GB',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    </div>
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 ">
    <?= $form->field($property, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Property Status']);?>
    </div>
    </div>
<input type="hidden" id="current-state-name" value="<?=$stateName;?>">
    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
    <?php echo $form->field($propertyAmenity, 'amenity_id')->checkboxList($allAmenities)->label('Amenities'); ?>
    </div>
    </div>
    <div class="form-group" style="text-align: center;" >
        <?= Html::submitButton($property->isNewRecord ? 'Create' : 'Update', ['class' => $property->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if(!$property->isNewRecord){?>
        <?= Html::a('Cancel', ['/mub-admin/real-estate/property','state' => $stateId], ['class' => 'btn btn-warning cancel'])?>
         <?php }?>
    </div>
</div>

<input type="hidden" id="current-state-id" value="<?=$stateId;?>">
    <?php ActiveForm::end(); ?>

</div>
<?php }else{?>
<div class="col-md-12">
    <h3>Please add amenities to start adding Properties</h3>
</div>
<?php }?>
