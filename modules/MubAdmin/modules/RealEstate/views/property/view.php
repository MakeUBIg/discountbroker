<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\fileupload\FileUploadUI;
use app\helpers\ImageUploader;
/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\RealEstate\propertys\Property */

$this->title = $property->property_name;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$this->params['breadcrumbs'][] = $this->title;

$propertyImages = new app\modules\MubAdmin\modules\RealEstate\models\PropertyImages();
$images = $propertyImages::find()->where(['property_id' => $property->id,'del_status' => '0'])->all();

?>
<style>
    .toggle
    {
        display:none;
    }
</style>
<div class="property-view">

<?= Html::a('Add/View Rooms', ['/mub-admin/real-estate/room', 'property' => $property->id,'state' => $stateId], ['class' => 'btn btn-info btn-lg pull-right'])?>
<div class="col-md-10">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-10">
    <?= FileUploadUI::widget([
    'model' => $propertyImages,
    'attribute' => 'url',
    'url' => ['property/property-image', 'state' => \Yii::$app->request->getQueryParam('state'),'property' => $property->id],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 5000000
    ],
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        ],
    ]); ?>
</div>
<div class="col-md-12">
    <?php if(!empty($images)){?>
 <?php  foreach($images as $key=>$image){
                    ?>

                    <div id="uploaded<?php echo $key;?>" style="display: inline; margin-right: 10px;">
                    <?php echo Html::img(ImageUploader::resizeRender($image->url, '100', '100'),['height'=>100,'width'=>100]);
                    echo Html::a(
                        Yii::t('app', 'x'), 
                        '#',
                        ["class" => "btn btn-danger realign","onClick" => "deleteFile('".$key."')"]);
                    ?>
                     </div>
                     <?php }}?>
 </div>
<div class="col-md-12" style="margin-top: 2em;">
    <?= DetailView::widget([
        'model' => $property,
        'attributes' => [
            'property_name',
            'property_type',
            'property_area',
            'description:ntext',
            'sa_a',
            'sa_b',
            'rooms_count',
        ],
    ]) ?>
    </div>
    
     
     <div class="col-md-12">
        <?= Html::a('Update', ['update', 'id' => $property->id,'state' => $stateId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $property->id,'state' => $stateId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('All Property', ['index','state' => $stateId], [
            'class' => 'btn btn-primary cancel',
        ]) ?>
     </div>
   

</div>