<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\RealEstate\propertys\Property */

$this->title = 'Create Property';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-create">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'property' => $property,
        'allAmenities' => $allAmenities,
        'propertyAmenity' => $propertyAmenity
    ]) ?>

</div>
