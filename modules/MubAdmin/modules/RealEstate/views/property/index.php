<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

   echo Breadcrumbs::widget([
      'homeLink' => [ 
                      'label' => Yii::t('yii', 'Dashboard'),
                      'url' => Yii::$app->homeUrl,
                 ],
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ]); 
$stateId = \Yii::$app->request->getQueryParam('state');

if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}

$state = new app\models\State();
$currentState = $state::findOne($stateId);
$this->title = 'Properties of '.$currentState->state_name;
$state = \Yii::$app->getRequest()->getQueryParam('state');
?>
<div class="property-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Property', ['create','state' => $state], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'property_name',
            'property_type',
            'property_area',
            'sa_a',
            'sa_b',
            'city_name',
            
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'active';
                    $id = $model->id;
                    $valInactive = 'inactive';
                    return ($model->status == 'inactive') ? '<button onClick="alert(setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valActive.'\''.','.'\''.$id.'\''.'))">Activate</button>' : '<button onClick="alert(setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valInactive.'\''.','.'\''.$id.'\''.'))">Deactivate</button>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                    
                },
            ],
            // 'city_name',
            // 'state_id',
            // 'property_slug',
            // 'description:ntext',
            // 'pincode',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['header' => 'Actions','class' => 'app\components\MubActionColumn'],
        ],
    ]); ?>
</div>
