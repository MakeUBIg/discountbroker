<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $bed app\modules\MubAdmin\modules\RealEstate\beds\Bed */

$this->title = $bed->bed_number;
$this->params['breadcrumbs'][] = ['label' => 'Beds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$roomId = \Yii::$app->request->getQueryParam('room');
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
?>
<div class="bed-view">
<div class="col-md-10 col-md-offset-1">
   <?= Html::a('<- Back to all beds',['index','state' => $stateId,'room' => $bed->room_id], [
            'class' => 'btn btn-danger',
        ]) ?> 
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $bed,
        'attributes' => [
            'id',
            'room_id',
            'available',
            'last_booked',
            'availability_date',
            'type',
            'del_status',
        ],
    ]) ?>
</div>
    <center>
    <div class="col-md-6 col-md-offset-1">
        <?= Html::a('Update', ['update', 'id' => $bed->id,'state' => $stateId,'room' => $roomId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $bed->id,'state' => $stateId,'room' => $roomId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Cancel',['index','state' => $stateId,'room' => $bed->room_id], [
            'class' => 'btn btn-warning cancel',
        ]) ?>
       </div>
    </center>
</div>
