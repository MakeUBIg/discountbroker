<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $bed app\modules\MubAdmin\modules\RealEstate\beds\BedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bed-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($bed, 'id') ?>

    <?= $form->field($bed, 'room_id') ?>

    <?= $form->field($bed, 'available') ?>

    <?= $form->field($bed, 'last_booked') ?>

    <?= $form->field($bed, 'availability_date') ?>

    <?php // echo $form->field($bed, 'type') ?>

    <?php // echo $form->field($bed, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
