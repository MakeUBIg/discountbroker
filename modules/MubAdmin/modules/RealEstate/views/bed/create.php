<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $bed app\modules\MubAdmin\modules\RealEstate\beds\Bed */

$this->title = 'Create Bed';
$this->params['breadcrumbs'][] = ['label' => 'Beds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bed-create">
    <div class="col-md-10 col-md-offset-1" >
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    </div>
    <?= $this->render('_form', [
        'bed' => $bed,
    ]) ?>

</div>
