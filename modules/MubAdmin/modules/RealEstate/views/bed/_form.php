<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $bed app\modules\MubAdmin\modules\RealEstate\beds\Bed */
/* @var $form yii\widgets\ActiveForm */

$roomId = \Yii::$app->request->getQueryParam('room');
?>

<div class="bed-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($bed, 'bed_number')->textInput(['maxlength' => '7']);?>

    <?= $form->field($bed, 'available')->dropDownList([ '0' => 'No', '1' => 'Yes', ], ['prompt' => 'Select'])->label('Is this bed available');?>

    <?= $form->field($bed,'availability_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options' => ['class'=> 'form-control']]) ?>

    <?= $form->field($bed, 'type')->dropDownList([ 'single' => 'Single', 'double' => 'Double', ], ['prompt' => 'Bed Type']) ?>

    <?= $form->field($bed, 'price')->textInput(['maxlength' => '5']);?>

    <?= $form->field($bed, 'room_id')->hiddenInput(['value' => $roomId])->label(false);?>

    <div class="form-group"><div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($bed->isNewRecord ? 'Create' : 'Update', ['class' => $bed->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div></div></div>
    <?php ActiveForm::end(); ?>
</div>