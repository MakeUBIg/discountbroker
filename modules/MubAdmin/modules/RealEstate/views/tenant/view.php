<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $tenant app\modules\MubAdmin\modules\RealEstate\tenants\Tenant */

$this->title = $tenant->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('All Tenants', ['/mub-admin/real-estate/tenant'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Update', ['update', 'id' => $tenant->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $tenant->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $tenant,
        'attributes' => [
            'id',
            'property_id',
            'name',
            'f_name',
            'email:email',
            'mobile',
            'f_mobile',
            'city_name',
            'monthly_rent',
            'security',
            'sa_home:ntext',
            'sa_office:ntext',
            'sa_permanent:ntext',
            'joining_date',
            'left_date'
        ],
    ]) ?>

</div>
