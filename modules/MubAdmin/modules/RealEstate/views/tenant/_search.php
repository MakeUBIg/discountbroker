<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $tenant app\modules\MubAdmin\modules\RealEstate\tenants\TenantSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($tenant, 'mub_user_id') ?>

    <?= $form->field($tenant, 'property_id') ?>

    <?= $form->field($tenant, 'name') ?>

    <?= $form->field($tenant, 'f_name') ?>

    <?php echo $form->field($tenant, 'email') ?>

    <?php // echo $form->field($tenant, 'mobile') ?>

    <?php // echo $form->field($tenant, 'f_mobile') ?>

    <?php // echo $form->field($tenant, 'state_id') ?>

    <?php // echo $form->field($tenant, 'city_name') ?>

    <?php // echo $form->field($tenant, 'monthly_rent') ?>

    <?php // echo $form->field($tenant, 'security') ?>

    <?php // echo $form->field($tenant, 'sa_home') ?>

    <?php // echo $form->field($tenant, 'sa_office') ?>

    <?php // echo $form->field($tenant, 'sa_permanent') ?>

    <?php // echo $form->field($tenant, 'joining_date') ?>

    <?php // echo $form->field($tenant, 'left_date') ?>

    <?php // echo $form->field($tenant, 'created_at') ?>

    <?php // echo $form->field($tenant, 'updated_at') ?>

    <?php // echo $form->field($tenant, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
