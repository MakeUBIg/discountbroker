<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\RealEstate\tenants\TenantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['tenant' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tenant', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            
            'email:email',
            'mobile',
            // 'f_mobile',
            // 'state_id',
            'city_name',
            'monthly_rent',
            // 'security',
            // 'sa_home:ntext',
            // 'sa_office:ntext',
            // 'sa_permanent:ntext',
            'joining_date',
            // 'left_date',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['header' => 'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
