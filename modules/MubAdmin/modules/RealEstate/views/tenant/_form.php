<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $tenant app\modules\MubAdmin\modules\RealEstate\tenants\Tenant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); 
    ?>

<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
<?= $form->field($tenant, 'property_id')->dropDownList($activeProperties, ['prompt' => 'Select A Property'])->label('Property Name') ?>
    </div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($tenant, 'name')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($tenant, 'f_name')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($tenant, 'email')->textInput(['maxlength' => true]) ?>
    </div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($tenant, 'mobile')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($tenant, 'f_mobile')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
<?= $form->field($tenant, 'state_id')->dropDownList($allStates, ['id' => 'mub-state','prompt' => 'Select A state']);?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($tenant, 'city_name')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($tenant, 'monthly_rent')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($tenant, 'security')->textInput() ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($tenant, 'sa_home')->textarea(['rows' => 4]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($tenant, 'sa_office')->textarea(['rows' => 4]) ?>
    </div>
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($tenant, 'sa_permanent')->textarea(['rows' => 4]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($tenant,'joining_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options' => ['class'=> 'form-control']]) ?>
</div>
<div class="col-md-5 col-sm-12 col-xs-12 ">
    <?= $form->field($tenant,'left_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd','options' => ['class'=> 'form-control']]) ?>
</div>
    <div class="form-group">
    <div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($tenant->isNewRecord ? 'Create' : 'Update', ['class' => $tenant->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
