<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $room app\modules\MubAdmin\modules\RealEstate\rooms\Room */
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$this->title = 'Update Room: ' . $room->room_number;
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $room->id, 'url' => ['view', 'id' => $room->id,'state' => $stateId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="room-update">
<div class="col-md-12 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'room' => $room,
    ]) ?>

</div>
