<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $room app\modules\MubAdmin\modules\RealEstate\rooms\Room */

$this->title = 'Create Room';
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-create">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'room' => $room,
    ]) ?>

</div>
