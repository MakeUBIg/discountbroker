<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchroom app\modules\MubAdmin\modules\RealEstate\rooms\RoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rooms';
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$state = new app\models\State();
$currentState = $state::findOne($stateId);
$propertyId = \Yii::$app->request->getQueryParam('property');
?>
<div class="room-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['room' => $searchroom]); ?>

    <p>
        <?= Html::a('<- Back to All Property', ['/mub-admin/real-estate/property/index','property' => $propertyId,'state' => $stateId], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Create Room', ['create','property' => $propertyId,'state' => $stateId], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'room_name',
            // 'beds_count',
            'room_number',
            'price',
            'room_type',
            // 'room_slug',
            // 'extrafield1',
            // 'extrafield2',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['header' => 'Actions','class' => 'app\components\MubActionColumn'],
        ],
    ]); ?>
</div>
