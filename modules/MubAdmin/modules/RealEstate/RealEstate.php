<?php

namespace app\modules\MubAdmin\modules\RealEstate;

class RealEstate extends \app\modules\MubAdmin\MubAdmin
{
    public $controllerNamespace = 'app\modules\MubAdmin\modules\RealEstate\controllers';

    public $defaultRoute = 'state';

    public function init()
    {
       parent::init();
    }
}
