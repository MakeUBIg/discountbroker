<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\RealEstate\models\Property;

/**
 * PropertySearch represents the model behind the search form about `app\modules\MubAdmin\modules\RealEstate\models\Property`.
 */
class PropertySearch extends Property
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'rooms_count', 'state_id', 'pincode'], 'integer'],
            [['lat', 'long', 'city_name', 'property_name', 'property_slug', 'property_type', 'property_area', 'sa_a', 'sa_b', 'description','status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $mubUserId = \app\models\User::getMubUserId();
        $stateId = \Yii::$app->request->getQueryParam('state');
        $query = Property::find()->where(['del_status' => '0','state_id' => $stateId]);

        if($mubUserId != '1')
        {
            $query->andWhere(['mub_user_id' => $mubUserId]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'rooms_count' => $this->rooms_count,
            'state_id' => $this->state_id,
            'pincode' => $this->pincode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'long', $this->long])
            ->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'property_name', $this->property_name])
            ->andFilterWhere(['like', 'property_slug', $this->property_slug])
            ->andFilterWhere(['like', 'property_type', $this->property_type])
            ->andFilterWhere(['like', 'property_area', $this->property_area])
            ->andFilterWhere(['like', 'sa_a', $this->sa_a])
            ->andFilterWhere(['like', 'sa_b', $this->sa_b])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'property.status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
