<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff;

/**
 * PropertySupportStaffSearch represents the model behind the search form about `app\modules\MubAdmin\modules\RealEstate\models\PropertySupportStaff`.
 */
class PropertySupportStaffSearch extends PropertySupportStaff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'property_id', 'mobile', 'state_id', 'salary', 'visite_charge'], 'integer'],
            [['staff_name', 'email', 'city_name', 'designation', 'experience', 'type', 'joining_date', 'left_date', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertySupportStaff::find()->where(['del_status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'property_id' => $this->property_id,
            'mobile' => $this->mobile,
            'state_id' => $this->state_id,
            'salary' => $this->salary,
            'visite_charge' => $this->visite_charge,
            'joining_date' => $this->joining_date,
            'left_date' => $this->left_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'staff_name', $this->staff_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
