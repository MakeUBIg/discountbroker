<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use app\models\MubUser;
use app\models\State;


/**
 * This is the model class for table "property_10".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $rooms_count
 * @property string $lat
 * @property string $long
 * @property string $city_name
 * @property integer $state_id
 * @property string $property_name
 * @property string $property_slug
 * @property string $property_type
 * @property string $property_area
 * @property string $sa_a
 * @property string $sa_b
 * @property string $description
 * @property integer $pincode
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property State $state
 * @property Room10[] $room10s
 */
class Property extends \app\components\Model
{
    public $room_price_show;
    public $show_price;
    public $distance;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    public $state_name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_name', 'sa_a', 'sa_b'], 'required'],
            [['mub_user_id', 'rooms_count', 'state_id', 'pincode'], 'integer'],
            [['property_type', 'property_category','property_area', 'description', 'status','del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['locality_name','lat', 'long', 'city_name', 'property_name', 'property_slug', 'sa_a', 'sa_b'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'rooms_count' => 'Rooms Count',
            'lat' => 'Lat',
            'long' => 'Long',
            'city_name' => 'City',
            'state_id' => 'State ID',
            'property_category' => 'Property Category',
            'property_name' => 'Property Name',
            'property_slug' => 'Property Slug',
            'property_type' => 'Property Type',
            'property_area' => 'Property For',
            'sa_a' => 'Address 1',
            'sa_b' => 'Address 2',
            'description' => 'Description',
            'pincode' => 'Pincode',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['property_id' => 'id'])->where(['room.del_status' => '0']);
    }

    public function getAmenities()
    {
        return $this->hasMany(PropertyAmenities::className(), ['property_id' => 'id'])->where(['property_amenities.del_status' => '0']);
    }

    public function getPropertyImages()
    {
        return $this->hasMany(PropertyImages::className(), ['property_id' => 'id'])->where(['property_images.del_status' => '0']);
    }
}
