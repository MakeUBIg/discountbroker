<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;

class RoomProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $room = new Room();
        $this->models = [
            'room' => $room
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'room' => $model
        ];
        return $this->relatedModels;
    }

    public function saveRoom($room)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $room->mub_user_id;
        }
        $room->mub_user_id =  $userId;
        $property = new Property();
        $forProperty = $property::findOne($room->property_id);
        $roomCounts = ($forProperty->rooms_count) ? $forProperty->rooms_count : 0;
        $roomCounts++;
        $forProperty->rooms_count = $roomCounts;
        if(!$forProperty->save())
        {
            p($forProperty->getErrors());
        }
        return ($room->save()) ?$room->id :p($room->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['room']))
            {
            try {
                    $roomId = $this->saveRoom($data['room']);
                    if($roomId)
                    {
                        return $roomId;
                    }
                    p('Room not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}