<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;

use app\models\MubUser;

/**
 * This is the model class for table "property_amenities_10".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property integer $amenity_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Amenity $amenity
 * @property MubUser $mubUser
 * @property Property10 $property
 */
class PropertyAmenities extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_amenities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amenity_id'], 'required'],
            [['mub_user_id', 'property_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'amenity_id' => 'Amenity ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmenity()
    {
        return $this->hasOne(Amenity::className(), ['id' => 'amenity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
