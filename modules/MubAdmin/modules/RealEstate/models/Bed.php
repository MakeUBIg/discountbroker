<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "bed_10".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $room_id
 * @property integer $bed_number
 * @property string $available
 * @property string $last_booked
 * @property string $availability_date
 * @property string $type
 * @property integer $price
 * @property string $extrafield1
 * @property string $extrafield2
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Room10 $room
 */
class Bed extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bed';
    }

    /**
     * @inheritdoc
    */
    public function rules()
    {
        return [
            [['room_id', 'price','available'], 'required'],
            [['mub_user_id', 'room_id', 'bed_number', 'price'],'integer','min' => 0],
            [['available', 'type', 'del_status'], 'string'],
            [['last_booked', 'availability_date', 'created_at', 'updated_at'], 'safe'],
            [['extrafield1', 'extrafield2'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'room_id' => 'Room ID',
            'bed_number' => 'Bed Number',
            'available' => 'Available',
            'last_booked' => 'Last Booked',
            'availability_date' => 'Availability Date',
            'type' => 'Type',
            'price' => 'Price',
            'extrafield1' => 'Extrafield1',
            'extrafield2' => 'Extrafield2',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }
}
