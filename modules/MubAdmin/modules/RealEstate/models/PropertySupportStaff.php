<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "property_support_staff".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property string $staff_name
 * @property string $email
 * @property integer $mobile
 * @property integer $state_id
 * @property string $city_name
 * @property integer $salary
 * @property string $designation
 * @property string $experience
 * @property integer $visite_charge
 * @property string $type
 * @property string $joining_date
 * @property string $left_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Property $property
 */
class PropertySupportStaff extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_support_staff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'staff_name', 'mobile'], 'required'],
            
            [['email'],'email'],
            [['mobile'],'number'],
            [['mobile'],'string', 'max'=> 10],

            
            [['mub_user_id', 'property_id', 'mobile', 'state_id', 'salary', 'visite_charge'], 'integer'],

            [['type', 'del_status'], 'string'],
            [['joining_date', 'left_date','support_staff_slug', 'created_at', 'updated_at'], 'safe'],

            [['staff_name', 'email'], 'string', 'max' => 50],
            [['city_name', 'designation', 'experience'], 'string', 'max' => 255],

            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],

            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'staff_name' => 'Staff Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'state_id' => 'State Name',
            'city_name' => 'City Name',
            'salary' => 'Salary',
            'designation' => 'Designation',
            'experience' => 'Experience',
            'visite_charge' => 'Visit Charge',
            'type' => 'Type',
            'joining_date' => 'Joining Date',
            'support_staff_slug' => 'Staff Slug',
            'left_date' => 'Left Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
