<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\RealEstate\models\Room;

/**
 * RoomSearch represents the model behind the search form about `app\modules\MubAdmin\modules\RealEstate\models\Room`.
 */
class RoomSearch extends Room
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'property_id', 'beds_count', 'price'], 'integer'],
            [['room_number', 'room_name', 'room_slug', 'room_type', 'extrafield1', 'extrafield2', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $mubUserId = \app\models\User::getMubUserId();
        $propertyId = \Yii::$app->request->getQueryParam('property');
        //redirect if no Property Id
        if(!$propertyId)
        {
            \Yii::$app->getResponse()->redirect('error');
        }
        $query = Room::find()->where(['del_status' => '0','property_id' => $propertyId]);

        if($mubUserId != '1')
        {
            $query->andWhere(['mub_user_id' => $mubUserId]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'property_id' => $this->property_id,
            'beds_count' => $this->beds_count,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'room_number', $this->room_number])
            ->andFilterWhere(['like', 'room_name', $this->room_name])
            ->andFilterWhere(['like', 'room_slug', $this->room_slug])
            ->andFilterWhere(['like', 'room_type', $this->room_type])
            ->andFilterWhere(['like', 'extrafield1', $this->extrafield1])
            ->andFilterWhere(['like', 'extrafield2', $this->extrafield2])
            ->andFilterWhere(['=', 'room.status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
