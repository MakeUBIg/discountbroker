<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class TenantProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $tenant = new Tenant();
        $this->models = [
            'tenant' => $tenant
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $property = new Property();
        $state = new \app\models\State();
        $mubUserId = \app\models\User::getMubUserId();
        $where = ['mub_user_id' => $mubUserId,'status' => 'active','del_status' => '0'];
        $activeProperties = $property->getAll('property_name',$where);
        $allStates = $state->getAll('state_name');
        $where = ['del_status' => '0'];
        return 
        [
        'allStates' => $allStates,
        'activeProperties' => $activeProperties
        ];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'tenant' => $model
        ];
        return $this->relatedModels;
    }

    public function saveTenant($tenant)
    {
        $userId = \app\models\User::getMubUserId();
        $tenant->mub_user_id =  $userId;
        $tenant->tenant_slug = \app\helpers\StringHelper::generateSlug($tenant->name);
        return ($tenant->save()) ?$tenant->id :p($tenant->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['tenant']))
            {
            try {
                    $tenantId = $this->saveTenant($data['tenant']);
                    if($tenantId)
                    {
                        return $tenantId;
                    }
                    p('tenant not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}