<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class BedProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $bed = new Bed();
        $this->models = [
            'bed' => $bed
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'bed' => $model
        ];
        return $this->relatedModels;
    }

    public function saveBed($bed)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $bed->mub_user_id;    
        }
        $bed->mub_user_id =  $userId;
        $room = new Room();
        $forRoom = $room::findOne($bed->room_id);
        $bedCounts = ($forRoom->beds_count) ? $forProperty->beds_count : 0;
        $bedCounts++;
        $forRoom->beds_count = $bedCounts;
        return ($bed->save()) ?$bed->id :p($bed->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['bed']))
            {
            try {
                    $bedId = $this->saveBed($data['bed']);
                    if($bedId)
                    {
                        return $bedId;
                    }
                    p('bed not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}