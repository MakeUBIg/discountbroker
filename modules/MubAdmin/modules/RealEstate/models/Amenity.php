<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;

use app\models\MubUser;

/**
 * This is the model class for table "amenity".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $amenity_category
 * @property string $amenity_name
 * @property string $amenity_slug
 * @property integer $amenity_price
 * @property string $status
 * @property string $icon_url
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Amenity extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'amenity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amenity_name', 'icon_url','amenity_category'], 'required'],
            [['mub_user_id', 'amenity_price'], 'integer'],
            [['amenity_category', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['amenity_name', 'amenity_slug', 'icon_url'], 'string', 'max' => 255],
            [['amenity_slug','amenity_name'], 'checkActive'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'amenity_category' => 'Amenity Category',
            'amenity_name' => 'Amenity Name',
            'amenity_slug' => 'Amenity Slug',
            'amenity_price' => 'Amenity Price',
            'status' => 'Status',
            'icon_url' => 'Icon Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
