<?php 

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use Yii;

class PropertyProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $property = new Property();
        $propertyAmenity = new PropertyAmenities();
        $this->models = [
            'property' => $property,
            'propertyAmenity' => $propertyAmenity,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $amenities = new Amenity();
        $condition = ['status' => 'active'];
        $allAmenities = $amenities->getAll('amenity_name',$condition);
        return [
            'allAmenities' => $allAmenities
        ];
    }

    public function getSavedAmenities($amenity)
    {
        $amenities = [];
        foreach ($amenity as $key => $amen) 
        {
            $amenities[] = $amen->amenity_id;
        }
        return $amenities;
    }

    public function getSavedImages($image)
    {
        $images = [];
        foreach ($image as $key => $img) 
        {
            $images[] = $img->url;
        }
        return $images;
    }

    public function getRelatedModels($model)
    {
        $propertyAmenities = $this->getSavedAmenities($model->amenities);

        if(!empty($propertyAmenities))
        {
            $propertyAmenity = $model->amenities[0];
            $propertyAmenity->amenity_id = $propertyAmenities;
        }
        else
        {
            $propertyAmenity = new PropertyAmenities();
        }

        $this->relatedModels = [
            'property' => $model,
            'propertyAmenity' => $propertyAmenity
        ];
        return $this->relatedModels;
    }

    public function saveProperty($property)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $property->mub_user_id;
        }
        $property->mub_user_id = $userId;
        $property->property_slug = \app\helpers\StringHelper::generateSlug($property->property_name);
        $stateId = \Yii::$app->request->getQueryParam('state');
        $property->state_id = $stateId;
        return ($property->save()) ? $property->id : p($property->getErrors());
    }

    public function savePropertyAmenities($propertyAmenity,$propertyId)
    {
        $selectedAmenities = $propertyAmenity->amenity_id;
        $mubUserId = \app\models\User::getMubUserId();
        $attribs = ['property_id','amenity_id','mub_user_id','created_at'];
        $recordSet = [];
        foreach ($selectedAmenities as $amenityId) {
            $recordSet[] = [$propertyId,$amenityId,$mubUserId,date('Y-m-d h:m:s',time())];
        }
        $amenCount = count($selectedAmenities);

        $propertyAmenity->deleteAll(['property_id' => $propertyId]);
        $inserted = Yii::$app->db->createCommand()->batchInsert($propertyAmenity->tableName(), $attribs, $recordSet)->execute();
        if($inserted == $amenCount)
        {
            return true;
        }
        p($propertyAmenity->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['property'])&&
            isset($data['propertyAmenity']))
            {
            try {
                   $propertyId = $this->saveProperty($data['property']);
                   if($propertyId)
                    {
                        $amenities = $this->savePropertyAmenities($data['propertyAmenity'],$propertyId);
                        if($amenities)
                        {
                            return $propertyId;
                        }
                        p($data['propertyAmenities']->getErrors());
                    }
                    p($data['property']->getErrors());
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}