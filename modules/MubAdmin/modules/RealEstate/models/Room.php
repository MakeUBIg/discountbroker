<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "room_10".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property integer $beds_count
 * @property string $room_number
 * @property string $room_name
 * @property string $room_size 
 * @property string $room_slug
 * @property string $room_type
 * @property integer $price
 * @property string $extrafield1
 * @property string $extrafield2
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Bed10[] $bed10s
 * @property MubUser $mubUser
 * @property Property10 $property
 */
class Room extends \app\components\Model
{
    public $bed_price_show;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_size','property_id', 'room_number', 'price'], 'required'],
            [['mub_user_id', 'property_id', 'beds_count', 'price'], 'integer','min' => 0],
            [['room_size','room_type', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['room_number', 'room_name', 'room_slug', 'extrafield1', 'extrafield2'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'beds_count' => 'Beds Count',
            'room_number' => 'Room Number',
            'room_name' => 'Room Name',
            'room_slug' => 'Room Slug',
            'room_size' => 'Room Size', 
            'room_type' => 'Room Type',
            'price' => 'Price',
            'extrafield1' => 'Extrafield1',
            'extrafield2' => 'Extrafield2',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBeds()
    {
        return $this->hasMany(Bed::className(), ['room_id' => 'id'])->where(['bed.del_status' => '0']);
    }

    public function getUnavailableBed()
    {
        return $this->hasMany(Bed::className(), ['room_id' => 'id'])->where(['bed.del_status' => '0','bed.available' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
