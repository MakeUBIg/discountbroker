<?php

namespace app\modules\MubAdmin\modules\RealEstate\models;
use app\models\MubUser;
use Yii;


/**
 * This is the model class for table "property_images_10".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $thumbnail_url
 * @property string $thumbnail_path
 * @property string $full_path
 * @property string $active
 * @property string $keyword
 * @property integer $width
 * @property integer $height
 * @property integer $display_width
 * @property integer $display_hieght
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Property10 $property
 */
class PropertyImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        $stateId = \Yii::$app->request->getQueryParam('state');
        if($stateId == '')
        {
            $stateId = \Yii::$app->request->getBodyParam('state');
        }
        return 'property_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'property_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['title', 'description','thumbnail_url', 'full_path'], 'required'],
            [['url'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png','maxFiles' => 10],
            [['active', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['description', 'url', 'thumbnail_url', 'thumbnail_path', 'full_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'title' => 'Title',
            'description' => 'Description',
            'url' => 'Url',
            'thumbnail_url' => 'Thumbnail Url',
            'thumbnail_path' => 'Thumbnail Path',
            'full_path' => 'Full Path',
            'active' => 'Active',
            'keyword' => 'Keyword',
            'width' => 'Width',
            'height' => 'Height',
            'display_width' => 'Display Width',
            'display_hieght' => 'Display Hieght',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property10::className(), ['id' => 'property_id']);
    }
}
