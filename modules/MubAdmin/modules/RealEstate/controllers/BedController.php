<?php

namespace app\modules\MubAdmin\modules\RealEstate\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\MubAdmin\modules\RealEstate\models\Bed;
use app\modules\MubAdmin\modules\RealEstate\models\BedSearch;
use app\modules\MubAdmin\modules\RealEstate\models\BedProcess;

class BedController extends MubController
{
   public function getPrimaryModel()
   {
        return new Bed();
   }

   public function getProcessModel()
   {
        return new BedProcess();
   }

   public function getSearchModel()
   {
        return new BedSearch();
   }
}
