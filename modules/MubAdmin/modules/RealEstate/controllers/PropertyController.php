<?php

namespace app\modules\MubAdmin\modules\RealEstate\controllers;

use app\components\MubController;
use app\modules\MubAdmin\modules\RealEstate\models\PropertySearch;
use app\modules\MubAdmin\modules\RealEstate\models\Property;
use app\modules\MubAdmin\modules\RealEstate\models\PropertyProcess;
use app\modules\MubAdmin\modules\RealEstate\models\PropertyImages;
use yii\web\UploadedFile;

use yii\helpers\FileHelper;
use yii\helpers\Json;

class PropertyController extends MubController
{
   public function getPrimaryModel()
   {
		  return new Property();
   }

   public function getProcessModel()
   {
      return new PropertyProcess();
   }

   public function getSearchModel()
   {
      return new PropertySearch();
   }

   public function updateUserRecord($postData)
    {
        $userModel = new \app\models\User();
        $mubUserModel = new \app\models\MubUser();
        $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
        $mubUserContact = $mubUser->mubUserContacts;
        $user = $userModel::findOne($mubUser->user_id);
        $user->first_name = $postData['MubUser']['first_name'];
        $user->last_name = $postData['MubUser']['last_name'];
        $user->setPassword($postData['MubUser']['password']);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        return ($user->save(false)) ? true : p($user->getErrors());
    }
    
    public function actionProfile()
    {   
        if (\Yii::$app->user->isGuest) {
            $this->redirect('/mub-admin');
        }
        else 
        {
            if(\Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/mub-admin/real-estate/property/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }



   public function actionPropertyImage($property)
   {
    if (\Yii::$app->request->isAjax) {
      $model = new PropertyImages();

      $imageFile = UploadedFile::getInstance($model, 'url');

      $directory = \Yii::getAlias('@app/images/properties') . DIRECTORY_SEPARATOR . \Yii::$app->request->getQueryParam('state') . DIRECTORY_SEPARATOR;
      if (!is_dir($directory)) {
          FileHelper::createDirectory($directory);
      }
      if ($imageFile) {
          $uid = uniqid(time(), true);
          $fileName = $uid . '.' . $imageFile->extension;
          $filePath = $directory . $fileName;
          if ($imageFile->saveAs($filePath)) {
              $path = '/images/properties/'.\Yii::$app->request->getQueryParam('state') . DIRECTORY_SEPARATOR.$fileName;
              $model->mub_user_id = \app\models\User::getMubUserId();
              $model->property_id = $property;
              $model->title = $uid . '.' . $imageFile->extension;
              $model->description = $imageFile->name.'.'.$imageFile->extension;
              $model->url = $path;
              $model->thumbnail_url = $path;
              $model->full_path = $path;
              if(!$model->save())
              {
                p($model->getErrors());
              }
              
              $successPath = '/images/done.png';
              return Json::encode([
                  'files' => [
                      [
                          'name' => $fileName,
                          'size' => $imageFile->size,
                          'url' => $successPath,
                          'thumbnailUrl' => $successPath,
                          'deleteUrl' => 'image-delete?name=' . $fileName,
                          'deleteType' => 'POST',
                      ],
                  ],
              ]);
          }
      }
      return '';
    }else
    {
      return 'go away';
    }
  }

  public function actionImageDelete($name)
  {
     if (\Yii::$app->request->isAjax) {
      $stateId = \Yii::$app->request->getQueryParam('state');
      $directory = \Yii::getAlias('@app/images/properties') . DIRECTORY_SEPARATOR . $stateId . DIRECTORY_SEPARATOR;
      if (is_file($directory.$name)) 
      {
          if(unlink($directory . $name))
          {
            $propertyImages = new \app\modules\MubAdmin\modules\RealEstate\models\PropertyImages();
            $success = $propertyImages::deleteAll(['title' => $name]);
            if($success)
            {
              return 'Image Deleted Successfully'; 
            }
          }else
          {
            return 'There was a problem deleting the Image';
          }
      }
      else
      {
        $propertyImages = new \app\modules\MubAdmin\modules\RealEstate\models\PropertyImages();
        $success = $propertyImages::deleteAll(['title' => $name]);
        if($success)
        {
          return 'Image Deleted from Records'; 
        }
      }
      
  }else
  {
    return 'Go Awayyyy!';
  }
}
}
