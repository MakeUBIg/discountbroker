<?php

namespace app\modules\MubAdmin\modules\RealEstate\controllers;

use Yii;
use app\modules\MubAdmin\modules\RealEstate\models\Amenity;
use app\modules\MubAdmin\modules\RealEstate\models\AmenityProcess;
use app\modules\MubAdmin\modules\RealEstate\models\AmenitySearch;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AmenityController implements the CRUD actions for Amenity model.
 */
class AmenityController extends MubController
{
   public function getPrimaryModel()
   {
        return new Amenity();
   }

   public function getProcessModel()
   {
        return new AmenityProcess();
   }

   public function getSearchModel()
   {
        return new AmenitySearch();
   }
}
