<?php

namespace app\modules\MubAdmin\modules\RealEstate\controllers;
use yii;
use app\models\State;

class StateController extends yii\web\Controller
{
	public $layout = "@app/views/layouts/admin";

    public function actionIndex()
    {
    	$state = new State();
    	$liveStates = $state::find()->where(['active' => '1'])->all();
    	return $this->render('index',['city_id' => 10,'states' => $liveStates]);
    }

}