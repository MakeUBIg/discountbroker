<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $mubUser app\models\MubUser */

$this->title = 'User Name:'.$mubUser->first_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-user-view">
<div class="col-md-8 col-md-offset-1">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $mubUser->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $mubUser->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'All User'), ['index'], ['class' => 'btn btn-primary']) ?>
    </p>
    </div>
<div class="col-md-8 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $mubUser,
        'attributes' => [
            
            'first_name',
            'last_name',
            ['attribute' => 'Mobile',
            'value' => $mubUserContacts->mobile],
            ['attribute' => 'Email',
            'value' => $mubUserContacts->email],
            ['attribute' => 'Address',
            'value' => $mubUserContacts->address],
            'username',
            'organization',
            
             ['attribute' => 'City',
            'value' =>function($model){
                $cityId = $model->mubUserContacts->city;
                $city = new \app\models\City();
                return  $cityName = $city::findOne($cityId)->city_name;
            }],
            ['attribute' => 'State',
            'value' =>function($model){
                $cityId = $model->mubUserContacts->city;
                $city = new \app\models\City();
                 $currentCity = $city::findOne($cityId);
                 return  $currentCity->state->state_name;
            }],
            'domain',
            'status'

        ],
    ]) ?>
</div>
</div>