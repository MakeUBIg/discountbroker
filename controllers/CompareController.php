<?php

namespace app\controllers;
use yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\MubAdmin\modules\broker\models\Brokers;
use app\modules\MubAdmin\modules\broker\models\BrokerProcess;

class CompareController extends \yii\web\Controller
{
	
    public function actionIndex()
    {
    	$this->layout = 'compare';
    	$brokers = new \app\modules\MubAdmin\modules\broker\models\Brokers();
    	return $this->render('index',['brokers' => $brokers]);
        // return $this->render('index');
    }
   


    public function actionEntry()
    {
    	$this->layout = 'compare';
        $params = Yii::$app->request->getQueryParams();
        $brokerProcess = new BrokerProcess();
        $brokersData = $brokerProcess->getComparedBrokers($params);
        $brokers = new \app\modules\MubAdmin\modules\broker\models\Brokers();
        return $this->render('index',['brokersData' => $brokersData,'brokers' => $brokers]);
        //p($brokersData);
    }



}
