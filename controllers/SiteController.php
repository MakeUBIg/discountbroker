<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $this->view->params['page'] = 'home';
       $allBroker = new \app\modules\MubAdmin\modules\broker\models\Brokers();
        $signupMail = new \app\models\SignupMail();
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $postImages = new \app\models\PostImages();
        $postComment = new \app\models\PostComment();
        return $this->render('index',['postModel' => $postModel,'mubUserModel' => $mubUserModel,'postImages' => $postImages,'signupMail' => $signupMail,'postComment' => $postComment, 'allBroker' => $allBroker]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionDirectory()
    {
        $brokers = new \app\modules\MubAdmin\modules\broker\models\Brokers();
        $brokerage = new \app\modules\MubAdmin\modules\broker\models\Brokerage();
        $transaction = new \app\modules\MubAdmin\modules\broker\models\TransactionCharge();
        $platforms = new \app\modules\MubAdmin\modules\broker\models\Platforms();
        $support = new \app\modules\MubAdmin\modules\broker\models\CustomersSupport();
        return $this->render('directory',['brokers' => $brokers,'brokerage' => $brokerage,'transaction' => $transaction,'platforms' => $platforms,'support' => $support]);
    }
    public function actionSlider()
    {
        return $this->render('slider');
    }
    public function actionBlogs()
    {
        return $this->render('blogs');
    }
    public function actionBlogDetails()
    {
        return $this->render('blog-details');
    }
    public function actionSidebar()
    {
        return $this->render('sidebar');
    }
    public function actionAllbroker()
    {
        $allBroker = new \app\modules\MubAdmin\modules\broker\models\Brokers();
        return $this->render('allbroker', ['allBroker' => $allBroker]);
    }
    public function actionBrokerdetails($id)
    {
        $singleBroker = new \app\modules\MubAdmin\modules\broker\models\Brokers();
        $brokers = $singleBroker::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleBrokerage = new \app\modules\MubAdmin\modules\broker\models\Brokerage();
        $brokerage = $singleBrokerage::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleCharting = new \app\modules\MubAdmin\modules\broker\models\Charting();
        $charting = $singleCharting::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleCustomerSupport = new \app\modules\MubAdmin\modules\broker\models\CustomersSupport();
        $customersupport = $singleCustomerSupport::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleFeatureSupportTools = new \app\modules\MubAdmin\modules\broker\models\FeatureSupportTools();
        $featuresupporttools = $singleFeatureSupportTools::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleInvestment = new \app\modules\MubAdmin\modules\broker\models\Investment();
        $investment =  $singleInvestment::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleMargins = new \app\modules\MubAdmin\modules\broker\models\Margins();
        $margins = $singleMargins::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singlePlans = new \app\modules\MubAdmin\modules\broker\models\Plans();
        $plans = $singlePlans::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singlePlatforms = new \app\modules\MubAdmin\modules\broker\models\Platforms();
        $platforms = $singlePlatforms::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleReporting = new \app\modules\MubAdmin\modules\broker\models\Reporting();
        $reporting = $singleReporting::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleStockBrokersFee = new \app\modules\MubAdmin\modules\broker\models\StockBrokersFee();
        $stockbrokersfee = $singleStockBrokersFee::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleTaxes = new \app\modules\MubAdmin\modules\broker\models\Taxes();
        $taxes = $singleTaxes::find()->where(['id' => $id,'del_status' => '0'])->one();

        $singleTransactionCharge = new \app\modules\MubAdmin\modules\broker\models\TransactionCharge();
        $transactioncharge = $singleTransactionCharge::find()->where(['id' => $id,'del_status' => '0'])->one();

        
        return $this->render('brokerdetails', [
            'transactioncharge' => $transactioncharge,
            'taxes' => $taxes,
            'stockbrokersfee' => $stockbrokersfee,
            'reporting' => $reporting,
            'platforms' => $platforms,
            'plans' => $plans,
            'margins' => $margins,
            'investment' => $investment,
            'featuresupporttools' => $featuresupporttools,
            'customersupport' => $customersupport,
            'charting' => $charting,
            'brokerage' => $brokerage,
            'brokers' => $brokers,
        ]);
    }
}
