<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/style.css',
        '/css/responsive/responsive.css',
    ];
    public $js = [
        '/js/jquery/jquery-2.2.4.min.js',
        '/js/bootstrap/popper.min.js',
        '/js/bootstrap/bootstrap.min.js',
        '/js/others/plugins.js',
        '/js/active.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\FontAwesomeAsset'        
    ];
}
