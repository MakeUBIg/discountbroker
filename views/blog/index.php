<?php 

use app\helpers\ImageUploader;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
 $postCategory = new \app\models\PostCategory();
    $mubCategory = new \app\models\MubCategory();
$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="breadcumb-area" style="background-image: url(/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="bradcumb-title text-center">
                        <h2>All Blogs</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcumb-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Blogs</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ****** Breadcumb Area End ****** -->

    <section class="archive-area section_padding_80">
        <div class="container">
            <div class="row">
    <!-- ****** Archive Area Start ****** -->
            <?php 
            foreach($allPosts as $post){
                $postDetail = $post->postDetail;
                $currentCategory = $postCategory::find()->where(['post_id' => $post->id,'del_status' => '0'])->one();
                $commentCount = $postComment::find()->where(['post_id' => $post->id,'del_status' => '0'])->andWhere(['<>','approved_by','NULL'])->count();

                $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
                ?>

                <!-- Single Post -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-post wow fadeInUp" data-wow-delay="0.1s">
                        <!-- Post Thumb -->
                   <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="/<?= $postImage->url;?>"  style="width: 450px;"></a><br/><br/>
                        <!-- Post Content -->
                        <div class="post-content">
                            <div class="post-meta d-flex">
                            <?php $mubUser = $mubUserModel::findOne($post->mub_user_id);?>
                                <div class="post-author-date-area d-flex">
                                    <!-- Post Author -->
                                    <div class="post-author">
                                        <a href="#"><?= $mubUser->username;?> </a>
                                    </div>
                                    <!-- Post Date -->
                                    <div class="post-date">
                                        <a href="#"><?= $post->created_at;?></a>
                                    </div>
                                </div>
                                <!-- Post Comment & Share Area -->
                                <div class="post-comment-share-area d-flex">
                                    <!-- Post Favourite -->
                                    <div class="post-favourite">
                                        <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= $postDetail->read_count;?></a>
                                    </div>
                                    <!-- Post Comments -->
                                    <div class="post-comments">
                                        <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> <?= ($commentCount == 0) ? 'No' : $commentCount;?></a>
                                    </div>
                                    <!-- Post Share -->
                                    <!-- <div class="post-share">
                                        <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    </div> -->
                                </div>
                            </div>
                            <a href="<?= '/blog/post-detail?id='.$post->url;?>"><h4 class="post-headline"><?=$post->post_title;?></h4>
                            </a>
                        </div>
                    </div>
                </div>            
                <?php }?>
                <div class="clearfix"></div>
            <div class="col-md-5"></div><?php if(!empty($allPosts)){?>
              <center><?= LinkPager::widget([
               'pagination' => $pages,
               ]);
              ?></center><?php }?>
            </div>
        </div>
</section>