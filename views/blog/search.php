<?php 
use app\helpers\ImageUploader;
use app\models\Post;
$postImages = new \app\models\PostImages();
$allBlogs = Post::find()->where(['status' => 'active','del_status' => '0'])->all();
?>
<div class="banner1">	
</div>
<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">

			<?php 
				if(!empty($blogs)){
			foreach($blogs as $blogDetail){	
				$blog = Post::findOne($blogDetail->post_id);
				$postImage = $postImages::find()->where(['post_id' => $blog->id])->one();
				?>
				<div class="rev-1">
				<?php if($postImage){?>
					<div class="rev-img">
						<a href="/blog/post-detail?id=<?= $blog->url; ?>"><img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '282', '132') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=282%C3%97300&w=282&h=132';?>" class="img-responsive" alt=""/></a>
					</div>
					<?php }?>
					<div class="<?=($postImage) ? 'rev-info' : ''?>">
						<h3><a href="/blog/post-detail?id=<?= $blog->url; ?>"><?= $blog->post_title;?></a></h3>
						<p><?= $blog->post_excerpt;?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php }}else{?>
				<div>
					<h3><a>No Blogs found</a></h3>
						<p>Sorry ! But we were not able to find any blog containing your Search String</p>
						<p>Following are some blogs you may be Interested In</p>
				</div>
				<div class="clearfix"></div>
				<?php }?>
			</div>
		</div>
		<!-- technology-right -->
		<div class="col-md-3 technology-right-1">
				<div class="blo-top">
					<div class="tech-btm">
					<img src="/images/newsletter.png" class="img-responsive" alt=""/>
					</div>
				</div>
				<div class="blo-top">
					<div class="tech-btm">
					<h4>Sign up to our newsletter</h4>
					<p>Pellentesque dui, non felis. Maecenas male</p>
						<div class="name">
							<form>
								<input type="text" placeholder="Email" required="">
							</form>
						</div>	
						<div class="button">
							<form>
								<input type="submit" value="Subscribe">
							</form>
						</div>
							<div class="clearfix"> </div>
					</div>
				</div>
				<div class="blo-top1">
					<div class="tech-btm">
					<h4>Top stories of the week </h4>
						<?php foreach($blogs as $blogDetail){
							$blog = Post::find()->where(['status' => 'active','id' => $blogDetail->post_id])->one();
							$postImage = $postImages::find()->where(['post_id' => $blog->id])->one();
							?>
						<div class="blog-grids">
							<div class="blog-grid-left">
								<a href="/blog/post-detail?id=<?= $blog->url; ?>"><img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '89', '85') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=282%C3%97300&w=89&h=85';?>" class="img-responsive" alt=""/></a>
							</div>
							<div class="blog-grid-right">
								<h5><a href="/blog/post-detail?id=<?= $blog->url; ?>"><?= $blog->post_title;?></a> </h5>
							</div>
							<div class="clearfix"> </div>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>