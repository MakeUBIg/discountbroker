<?php 

$postComment = new \app\models\PostComment();
$postCategory = new \app\models\PostCategory();
$mubCategory = new \app\models\MubCategory();
$currentCategory = $postCategory::find()->where(['post_id' => $post->id,'del_status' => '0'])->one();

use app\models\Post;
use yii\helpers\Html;
use app\helpers\ImageUploader;
use yii\widgets\ActiveForm;
	$this->title = 'Title from view';
	$this->title = $this->title ? $this->title : 'default title';

/* facebook meta tag*/
	$this->registerMetaTag([
    	'title' => 'og:title',
    	'content' => $post->post_title,	
	]);

	$this->registerMetaTag([
    	'app_id' => 'fb:app_id',
    	'content' => '1872999093024677'
	]);

	$this->registerMetaTag([
    	'type' => 'og:type',
    	'content' => 'article'
	]);

	$this->registerMetaTag([
    	'url' => 'og:url',
    	'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
	]);
	if($postImage){
		$this->registerMetaTag([
	    	'image' => 'og:image',
	    	'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads'.$postImage->url,
		]);
	}
	$this->registerMetaTag([
    	'description' => 'og:description',
    	'content' => 'description here'
	]);

/*close facebook meta tag*/

/*twitter meta tag*/

	$this->registerMetaTag([
    	'card' => 'twitter:card',
    	'content' => "summury"
	]);
	$this->registerMetaTag([
    	'site' => "twitter:site",
    	'content' => "@publisher_handle"
	]);
	
	$this->registerMetaTag([
    	'title' => 'twitter:title',
    	'content' => 'page title'
	]);

	$this->registerMetaTag([
    	'description' => 'twitter:description',
    	'content' => 'Page description less than 200 characters'	
	]);

	$this->registerMetaTag([
    	'creater' => 'twitter:creater',
    	'content' => '@author_handle'	
	]);

	$this->registerMetaTag([
    	'url' => 'twitter:url',
    	'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
	]);
	if($postImage){
	$this->registerMetaTag([
	    	'image' => 'twitter:image:src',
	    	'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads'.$postImage->url,
		]);
	}

$this->title = 'Discount Brokers Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="breadcumb-area" style="background-image: url(/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="bradcumb-title text-center">
                        <h2><?=$post->post_title;?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcumb-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Archive</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Single Post Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ****** Breadcumb Area End ****** -->

    <!-- ****** Single Blog Area Start ****** -->
    <section class="single_blog_area section_padding_80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="row no-gutters">

                        <!-- Single Post Share Info -->
                        <div class="col-2 col-sm-1">
                            <div class="single-post-share-info mt-100">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#" class="googleplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#" class="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-10 col-sm-11">
                            <div class="single-post">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                    <img src="../<?= $postImage->url;?>" style="width: 650px;">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-meta d-flex">
                                        <div class="post-author-date-area d-flex">
                                            <!-- Post Author -->
                                            <div class="post-author">
                                                <a href="#"><?= $mubUser->username;?></a>
                                            </div>
                                            <!-- Post Date -->
                                            <div class="post-date">
                                                <a href="#"><?= $post->created_at;?></a>
                                            </div>
                                        </div>
                                        <!-- Post Comment & Share Area -->
                                        <div class="post-comment-share-area d-flex">
                                            <!-- Post Favourite -->
                                            <div class="post-favourite">
                                                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= $postDetail->read_count;?></a>
                                            </div>
                                            <!-- Post Comments -->
                                            <div class="post-comments">
                                                <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i><?= ($comments['comments_count'] == 0) ? 'No ' : $comments['comments_count'];?></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <p><?= $postDetail->post_content; ?></p>
                                                               
                                </div>
                            </div>

                            <?php if($comments['comments']){?>
                            <div class="comment_area section_padding_50 clearfix">
                                <h1 class="mb-30">Comments</h1>
                                <?php foreach($comments['comments'] as $cmt){?>
                                <ol>
                                    <!-- Single Comment Area -->
                                    <li class="single_comment_area">
                                        <div class="comment-wrapper d-flex">
                                            <!-- Comment Meta -->
                                            <div class="comment-author">
                                                <img src="img/blog-img/19.jpg" alt="">
                                            </div>
                                            <!-- Comment Content -->
                                            <div class="comment-content">
                                                <span class="comment-date text-muted"><?=$cmt->created_at;?></span>
                                                <h5<?=$cmt->name;?></h5>
                                                <p><?=$cmt->comment_text;?></p>
                                                <!-- <a href="#">Like</a>
                                                <a class="active" href="#">Reply</a> -->
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                           		<?php }?>
                            </div>
							<?php }?>
                            <!-- Leave A Comment -->
                            <div class="leave-comment-area section_padding_50 clearfix">
                                <div class="comment-form">
                                    <h2 class="mb-30">Leave A Comment</h2>

                                    <!-- Comment Form -->
                                    <div class=" comment-bottom">
                                    <?php $form = ActiveForm::begin(['id' => 'user_comment']);?>
                                            <?= $form->field($postComment, 'name')->textInput(['maxlength'=> 250,'placeholder' => "Enter Your Name"])->label(false);  ?>
                                            <?= $form->field($postComment, 'email')->textInput(['maxlength'=> 250,'placeholder' => "Enter Your Email"])->label(false); ?>
                                            <?= $form->field($postComment, 'comment_text')->textArea(['cols'=>'6','placeholder' => "Enter Your Comment"])->label(false); ?>
                                            <?= $form->field($postComment, 'url')->hiddenInput(['value' => \Yii::$app->request->get('id')])->label(false) ?>
                                            <button type="submit" class="btn contact-btn"> POST</button>
                                    <?php ActiveForm::end();?>
                    </div>
                                   
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
				<div class="col-12 col-sm-8 col-md-6 col-lg-4">
                    <div class="blog-sidebar mt-5 mt-lg-0">
                        <div class="single-widget-area popular-post-widget">
                            <div class="widget-title text-center">
                                <h6>Populer Post</h6>
                            </div><br/>
                         <?php 
                        $allPosts = $postModel::find()->where(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC])->limit(20)->all();
                        foreach($allPosts as $post){
                            $postDetail = $post->postDetail;
                           
                            $commentCount = $postComment::find()->where(['post_id' => $post->id,'del_status' => '0'])->andWhere(['<>','approved_by','NULL'])->count();
                                $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
                            ?>
                        <div class="single-populer-post d-flex">
                            <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="../<?= $postImage->url;?>" style="width: 130px;"></a>
                            <div class="post-content">
                                <a href="<?= '/blog/post-detail?id='.$post->url;?>">
                                    <h5><?=$post->post_title;?></h5>
                                </a>
                                <p><?= $mubUser->username;?> </p>
                                <p><?= $post->created_at;?></p>

                            </div>
                        </div>
                          <?php }?>
                           <!-- <div class="single-widget-area newsletter-widget">
                            <div class="widget-title text-center">
                                <h6>Newsletter</h6>
                            </div>
                            <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.</p>
                            <div class="newsletter-form">
                                <form action="#" method="post">
                                    <input type="email" name="newsletter-email" id="email" placeholder="Your email">
                                    <button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div> -->
                    </div> 
                    </div> 
                </div></div></div></section>