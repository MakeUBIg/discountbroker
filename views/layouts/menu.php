<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}

if(\Yii::$app->user->can('brokeritem/index'))
{
    $menu[] = ["label" => "Brokers", "url" => "/mub-admin/broker/brokeritem/", "icon" => "cog"];        
}


if(\Yii::$app->user->can('album/index'))
{
$menu[] = [
            "label" => "Gallery", 
            "url" => ["/mub-admin/gallery"], 
            "icon" => "picture-o"
        ];
}

if(\Yii::$app->user->can('blogitem/index'))
{
   //getting data by role
   $role = \app\components\Model::getAuthRole();
   $posts = new \app\models\Post();
   $postComments = new \app\models\PostComment();
   
   if($role == 'subadmin')
   {
       $mubUserId = \app\models\User::getMubUserId();
       $count = $posts::find()->where(['del_status' => '0','mub_user_id' => $mubUserId])->count();
       $unreadComments = \app\models\PostComment::find()->where(['post_comment.del_status' => '0','post.mub_user_id' => $mubUserId])->joinWith('post')->count();
   }
   else 
   {
    $count = $posts::find()->where(['del_status' => '0'])->count();
       $unreadComments = $postComments::find()->where(['del_status' => '0','read' => '0'])->count();
   }        

$menu[] = [
"label" => "Blogs",
       "url" => "#",
       "icon" => "bold",
       "items" => [
           [
               "label" => "Posts",
               "url" => ["/mub-admin/blog"],
               "badge" => $count,
           ],
           [
               "label" => "Category",
               "url" => ["/mub-admin/blog/category"],
               // "badge" => $count,
           ],
           [
               "label" => "Comments",
               "url" => ["/mub-admin/blog/comments"],
               "badge" => $unreadComments,
               "badgeOptions" => ["class" => ($unreadComments!='0') ?"label-success" : ''],
           ],
       ],];
}

if(\Yii::$app->user->can('user/index'))
{
     $userss = new  app\models\MubUser();

    $newCount = $userss::find()->where(['del_status' => '0','status' => 'inactive'])->count();
    $userssCount = $userss::find()->where(['del_status' => '0','status' => 'active'])->count();
    $menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user",
                "badge" => ($newCount > 0) ? $newCount : $userssCount,
                "badgeOptions" => ["class" => ($newCount > 0) ?"label-success" : ''],
                "icon" => "cog"
            ];
}

?>
