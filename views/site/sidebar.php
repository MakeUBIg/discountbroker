
    <div class="single-widget-area popular-post-widget">
        <div class="widget-title text-center">
            <h6>Populer Post</h6>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/1.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Top Wineries To Visit In England</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/2.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>The 8 Best Gastro Pubs In Bath</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/3.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Zermatt Unplugged the best festival</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/3.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Zermatt Unplugged the best festival</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/3.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Zermatt Unplugged the best festival</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post --> <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/3.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Zermatt Unplugged the best festival</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/3.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Zermatt Unplugged the best festival</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/4.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Harrogate's Top 10 Independent Eats</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
        <!-- Single Popular Post -->
        <div class="single-populer-post d-flex">
            <img src="/img/sidebar-img/5.jpg" alt="">
            <div class="post-content">
                <a href="#">
                    <h6>Eating Out On A Budget In Oxford</h6>
                </a>
                <p>Tuesday, October 3, 2017</p>
            </div>
        </div>
    </div>

    <!-- Single Widget Area -->
    
    <!-- Single Widget Area -->
    <div class="single-widget-area newsletter-widget">
        <div class="widget-title text-center">
            <h6>Newsletter</h6>
        </div>
        <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.</p>
        <div class="newsletter-form">
            <form action="#" method="post">
                <input type="email" name="newsletter-email" id="email" placeholder="Your email">
                <button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </form>
        </div>
    </div>
</div>