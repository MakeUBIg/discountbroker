<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
     
     @media (min-width: 1200px) {
        .container {
         width: 100%;
            }
                }
                .carousel{
                margin-top: -2em;
            }
            
            .carousel-caption{
                top:35%;
            }
            .carousel-indicators
            {
                display: none;
            }
            .colo{
                color: #fff;
                background-color: #b180808a;
                font-size:40px;
                width: 70%;
            }
    

     @media (max-width: 600px) {
        .colo{
                color: #fff;
                background-color: #b180808a;
                font-size:25px;
                width: 100%;
            }
     }

  </style>
</head>
<body>

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="/img/blog-img/slider/TToo.jpg" alt="Los Angeles" style="width:100%; height: 550px;">
        <div class="carousel-caption">
          <h1 class="colo">Los Angeles</h1>
          <h4 class="colo">LA is always so much fun!</h4>
        </div>
      </div>

      <div class="item">
        <img src="/img/blog-img/slider/Doors.jpg" alt="Chicago" style="width:100%; height: 550px;">
        <div class="carousel-caption">
          <h1 class="colo">Los Angeles</h1>
          <h4 class="colo">LA is always so much fun!</h4>
        </div>
      </div>
    
      <div class="item">
        <img src="/img/blog-img/slider/Hourglass.jpg" alt="New York" style="width:100%; height: 550px;">
        <div class="carousel-caption">
          <h1 class="colo">Los Angeles</h1>
          <h4 class="colo">LA is always so much fun!</h4>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</body>
</html>
