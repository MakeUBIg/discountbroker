
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\helpers\ImageUploader;
use yii\widgets\ActiveForm;
use app\modules\MubAdmin\modules\broker\models\Brokers;

$singleBrokerage = new \app\modules\MubAdmin\modules\broker\models\Brokerage();
$singleCharting = new \app\modules\MubAdmin\modules\broker\models\Charting();
$singleCustomerSupport = new \app\modules\MubAdmin\modules\broker\models\CustomersSupport();
$singleFeatureSupportTools = new \app\modules\MubAdmin\modules\broker\models\FeatureSupportTools();      
$singleInvestment = new \app\modules\MubAdmin\modules\broker\models\Investment();
$singleMargins = new \app\modules\MubAdmin\modules\broker\models\Margins();      
$singlePlans = new \app\modules\MubAdmin\modules\broker\models\Plans();     
$singlePlatforms = new \app\modules\MubAdmin\modules\broker\models\Platforms();
$singleReporting = new \app\modules\MubAdmin\modules\broker\models\Reporting();
$singleStockBrokersFee = new \app\modules\MubAdmin\modules\broker\models\StockBrokersFee();
$singleTaxes = new \app\modules\MubAdmin\modules\broker\models\Taxes();
$singleTransactionCharge = new \app\modules\MubAdmin\modules\broker\models\TransactionCharge();
 
$this->title = 'Discount Brokers Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    table, th, td {
  border: 1px solid black !important;
  border-collapse: collapse!important;
     padding: 5px!important;
     font-size: 15px;
     text-align: center;
     }
.tablewidth{
    width:100%;
}
 .textalign{
    text-align: center;
}    
</style>
    <div class="breadcumb-area" style="background-image: url(/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="bradcumb-title text-center">
                        <h2 class="post-headline"><?= $brokers->broker_name?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcumb-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Bokers</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $brokers->broker_name?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ****** Breadcumb Area End ****** -->

    <!-- ****** Single Blog Area Start ****** -->
    <section class="single_blog_area section_padding_80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    <div class="row no-gutters">

                        <!-- Single Post Share Info -->
                        <div class="col-2 col-sm-1">
                            <div class="single-post-share-info mt-100">
                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#" class="googleplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#" class="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        <!-- Single Post -->
                        <div class="col-10 col-sm-11">
                            <div class="single-post">
                                <!-- Post Thumb -->
                                
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-meta d-flex">
                                        <div class="post-author-date-area d-flex">
                                            <!-- Post Author -->
                                            
                                            <!-- Post Date -->
                                            
                                        </div>
                                        <!-- Post Comment & Share Area -->
                                        <div class="post-comment-share-area d-flex">
                                            <!-- Post Favourite -->
                                            
                                            <!-- Post Comments -->
                                           
                                            <!-- Post Share -->
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                      <div class="col-md-3">
                                      </div>
                                       <div class="col-md-6">
                                        <img src=" <?= '/'.$brokers->image;?>" style="width: 400px; margin-top: -10em;
    margin-bottom: -6em;">
                                      </div>
                                       <div class="col-md-2">
                                      </div>
                                     </div> 
                                    <div class="row">
                                    <div class="col-md-12"> 
                                     <?= $brokers->broker_summary;?>
                                       <br/><br/>
                                   </div>
                                   </div>

                                    
                                   
                                   
                                </div>
                            </div>
                            
                            <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Background</h2></th>
                          </tr>
                        <tr>
                           <td><B>Incorporation’s year<B></td>
                           <td><?= $brokers->incorporation_year;?></td>
                           </tr>
                           <tr>
                               <td><B>Type of Broker </B></td>
                           <td><?= $brokers->type_of_broker;?></td>
                           </tr>
                           <tr>
                               <td><B>Account Type</B></td>
                               <td><?= $brokers->account_type;?></td>
                           </tr>
                          
                           <tr>
                               <td><B>Supported Exchanges</B> </td>
                               <td><?= $brokers->supported_exchanges;?></td>
                           </tr>
                           <tr>
                               <td><B>Member of (NSDL/CDSL)</B></td>
                               <td><?= $brokers->member_of;?></td>
                           </tr>
                     </table><br><br>

                           <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Brokerage</h2></th>
                          </tr>
                        <tr>
                           <td><B>Equity</B></td>
                           <td><?= $brokerage->equity;?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Futures </B></td>
                           <td><?= $brokerage->equity_futures;?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Options</B></td>
                               <td><?= $brokerage->equity_options;?></td>
                           </tr>
                           <tr>
                               <td><B>Currency Futures</B></td>
                               <td><?= $brokerage->currency_futures;?></td>
                           </tr>
                           <tr>
                               <td><B>Currency Options</B> </td>
                               <td><?= $brokerage->currency_options;?></td>
                           </tr>
                           <tr>
                               <td><B>Commodity</B></td>
                               <td><?= $brokerage->commodity;?></td>
                           </tr>
                           <tr>
                               <td><B>Other charges</B></td>
                               <td><?= $brokerage->other_charges;?></td>
                           </tr>

                       </table><br><br>
                        <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Stozk Broker Fees / Charges</h2></th>
                          </tr>
                        <tr>
                           <td><B>Trading only</B></td>
                           <td><?= $stockbrokersfee->trading_only?></td>
                           </tr>
                           <tr>
                               <td><B>Trading & Demat</B></td>
                           <td><?= $stockbrokersfee->trading_Demat?></td>
                           </tr>
                           <tr>
                               <td><B>Demat Account AMC </B></td>
                               <td><?= $stockbrokersfee->demat_account_amc?></td>
                           </tr>
                           <tr>
                               <td><B>Commodity</B></td>
                               <td><?= $stockbrokersfee->commodity?></td>
                           </tr>
                           <tr>
                               <td><B>DP Transaction Charge</B> </td>
                               <td><?= $stockbrokersfee->dp_transaction?></td>
                           </tr>
                           <tr>
                               <td><B>Offline order placing Charge</B></td>
                               <td><?= $stockbrokersfee->offline_order_placing?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Taxes</B></th>
                          </tr>
                        <tr>
                           <td><B>Securities Transaction Tax (STT)</B></td>
                           <td><?= $taxes->securities_transaction?></td>
                           </tr>
                           <tr>
                               <td><B> Exchange Transaction Charge</B></td>
                                <td><?= $taxes->exchange_transaction?></td>
                           </tr>
                           <tr>
                               <td><B>SEBI Charges</B></td>
                               <td><?= $taxes->sebi_charges?></td>
                           </tr>
                           <tr>
                               <td><B>Goods and Services Tax (GST)</B></td>
                               <td><?= $taxes->goods_services?></td>
                           </tr>
                           <tr>
                               <td><B>Stamp Duty</B> </td>
                               <td><?= $taxes->stamp_duty?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Transaction Charges</h2></th>
                          </tr>
                        <tr>
                           <td><B>Equity Delivery</B></td>
                           <td><?= $transactioncharge->equity_delivery?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Intraday</B></td>
                               <td><?= $transactioncharge->equity_intraday?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Futures</B></td>
                               <td><?= $transactioncharge->equity_futures?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Options </B></td>
                               <td><?= $transactioncharge->equity_options?></td>
                           </tr>
                           <tr>
                               <td><B>Currency Futures </B> </td>
                               <td><?= $transactioncharge->currency_futures?></td>
                           </tr>
                            <tr>
                               <td><B>Currency Options </B> </td>
                               <td><?= $transactioncharge->currency_options?></td>
                           </tr>
                            <tr>
                               <td><B>Commodity</B> </td>
                               <td><?= $transactioncharge->commodity?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Margins</h2></th>
                          </tr>
                        <tr>
                           <td><B>Equity Delivery</B></td>
                           <td><?= $margins->m_equity_delivery?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Futures</B></td>
                           <td><?= $margins->m_equity_features?></td>
                           </tr>
                           <tr>
                               <td><B>Equity Options</B></td>
                               <td><?= $margins->m_equity_options?></td>
                           </tr>
                           <tr>
                               <td><B>Currency Futures  </B></td>
                               <td><?= $margins->m_currency_features?></td>
                           </tr>
                           <tr>
                               <td><B>Currency Options</B> </td>
                               <td><?= $margins->m_currency_options?></td>
                           </tr>
                            <tr>
                               <td><B>Commodity </B> </td>
                               <td><?= $margins->m_commodity?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Platforms</h2></th>
                          </tr>
                        <tr>
                           <td><B>Software</B></td>
                           <td><?= $platforms->software?></td>
                           </tr>
                           <tr>
                               <td><B>Web/HTML 5</B></td>
                           <td><?= $platforms->web_html?></td>
                           </tr>
                           <tr>
                               <td><B>Mobile</B></td>
                               <td><?= $platforms->mobile?></td>
                           </tr>
                           <tr>
                               <td><B>Trading Platform  </B></td>
                               <td><?= $platforms->trading_platform?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Charting</h2></th>
                          </tr>
                        <tr>
                           <td><B>Intraday</B></td>
                           <td><?= $charting->intraday?></td>
                           </tr>
                           <tr>
                               <td><B>End of Day</B></td>
                           <td><?= $charting->end_of_day?></td>
                           </tr>
                           <tr>
                               <td><B>Coding/Backtesting</B></td>
                               <td><?= $charting->coding_backtesting?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Brokerages plan </h2></th>
                          </tr>
                        <tr>
                           <td><B>PLAN 1</B></td>
                           <td><?= $plans->plan_1?></td>
                           </tr>
                           <tr>
                               <td><B>PLAN 2</B></td>
                           <td><?= $plans->plan_2?></td>
                           </tr>
                           <tr>
                               <td><B>PLAN 3</B></td>
                               <td><?= $plans->plan_3?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Reporting </h2></th>
                          </tr>
                        <tr>
                           <td><B>Online Trade Reports</B></td>
                           <td><?= $reporting->online_trade?></td>
                           </tr>
                           <tr>
                               <td><B>Online PNL Reports</B></td>
                           <td><?= $reporting->online_pnl?></td>
                           </tr>
                           <tr>
                               <td><B>Online Contract Notes</B></td>
                               <td><?= $reporting->online_contract?></td>
                           </tr>
                           <tr>
                               <td><B>Daily Market Report</B></td>
                               <td><?= $reporting->daily_market?></td>
                           </tr>
                       </table><br><br>
                           <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Feature/ Convenience/ Support & Tools</h2></th>
                          </tr>
                        <tr>
                           <td><B>Good Till Cancelled (GTC) </B></td>
                           <td><?= $featuresupporttools->good_till_cancelled?></td>
                           </tr>
                           <tr>
                               <td><B>Online Demo</B></td>
                           <td><?= $featuresupporttools->online_demo?></td>
                           </tr>
                           <tr>
                               <td><B>Online Portfolio</B></td>
                               <td><?= $featuresupporttools->online_portfolio?></td>
                           </tr>
                           <tr>
                               <td><B>Instant Fund withdrawal </B></td>
                               <td><?= $featuresupporttools->instant_fund_withdrawal?></td>
                           </tr>
                           <tr>
                               <td><B>Relationship Managers </B> </td>
                               <td><?= $featuresupporttools->relationship_managers?></td>
                           </tr>
                            <tr>
                               <td><B>Research & Tips </B> </td>
                               <td><?= $featuresupporttools->research_tips?></td>
                           </tr>
                            <tr>
                               <td><B>Brokerage Calculator</B> </td>
                               <td><?= $featuresupporttools->brokerage_calculator?></td>
                           </tr>
                           <tr>
                               <td><B>Margin Calculator</B></td>
                           <td><?= $featuresupporttools->margin_alculator?></td>
                           </tr>
                           <tr>
                               <td><B>Bracket orders & Trailing Stoploss</B></td>
                               <td><?= $featuresupporttools->bracket_orders?></td>
                           </tr>
                           <tr>
                               <td><B>Cover order </B></td>
                               <td><?= $featuresupporttools->cover_order?></td>
                           </tr>
                           <tr>
                               <td><B>Training & Education </B> </td>
                               <td><?= $featuresupporttools->training_sducation?></td>
                           </tr>
                            <tr>
                               <td><B>3 in 1 Account </B> </td>
                               <td><?= $featuresupporttools->in_account?></td>
                           </tr>
                            <tr>
                               <td><B>Mobile Trading</B> </td>
                               <td><?= $featuresupporttools->mobile_trading?></td>
                           </tr>
                           <tr>
                               <td><B>Trading Platform</B> </td>
                               <td><?= $featuresupporttools->trading_platform?></td>
                           </tr>
                           <tr>
                               <td><B>Intraday Square-off Time</B> </td>
                               <td><?= $featuresupporttools->intraday_square?></td>
                           </tr>
                           <tr>
                               <td><B>Free Tips</B> </td>
                               <td><?= $featuresupporttools->free_tips?></td>
                           </tr>
                           <tr>
                               <td><B>News Alerts </B> </td>
                               <td><?= $featuresupporttools->news_alerts?></td>
                           </tr>  
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Investment Options </h2></th>
                          </tr>
                        <tr>
                           <td><B>Stock / Equity</B></td>
                           <td><?= $investment->stock_equity?></td>
                           </tr>
                           <tr>
                               <td><B>Commodity</B></td>
                           <td><?= $investment->commodity?></td>
                           </tr>
                           <tr>
                               <td><B>Currency</B></td>
                               <td><?= $investment->currency?></td>
                           </tr>
                           <tr>
                               <td><B>Initial Public Offers (IPO)</B></td>
                               <td><?= $investment->initial_public_offers?></td>
                           </tr>
                          <tr>
                           <td><B>Mutual Funds</B></td>
                           <td><?= $investment->mutual_funds?></td>
                           </tr>
                           <tr>
                               <td><B>Bond / NCD</B></td>
                           <td><?= $investment->bond_ncd?></td>
                           </tr>
                           <tr>
                               <td><B>Debt</B></td>
                               <td><?= $investment->debt?></td>
                           </tr>
                           <tr>
                               <td><B>Investor category</B></td>
                               <td><?= $investment->investor_category?></td>
                           </tr>
                       </table><br><br>
                       <table class="tablewidth">
                       <tr>
                          <th colspan="2"><h2>Customer Support </h2></th>
                          </tr>
                        <tr>
                           <td><B>24/7 Customer Service</B></td>
                           <td><?= $customersupport->customer_service?></td>
                           </tr>
                           <tr>
                               <td><B>Email Support</B></td>
                           <td><?= $customersupport->email_support?></td>
                           </tr>
                           <tr>
                               <td><B>Online Live Chat </B></td>
                               <td><?= $customersupport->online_live_chat?></td>
                           </tr>
                           <tr>
                               <td><B>Phone Support</B></td>
                               <td><?= $customersupport->phone_support?></td>
                           </tr>
                          <tr>
                           <td><B>Toll Free Number</B></td>
                           <td><?= $customersupport->toll_free_number?></td>
                           </tr>
                           <tr>
                               <td><B>Through Branches</B></td>
                           <td><?= $customersupport->through_branches?></td>
                           </tr>
                       </table>   
                        </div>
                    </div>
                </div>
                
              
            </div>
        </div>
</section>