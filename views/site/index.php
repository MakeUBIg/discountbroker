<?php 

use app\helpers\ImageUploader;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$allBroker = new app\modules\MubAdmin\modules\broker\models\Brokers();
$brokerdetails = $allBroker::find()->where(['del_status' => '0'])->limit(12)->orderBy(['id'=>SORT_DESC])->all();
$brokerdetail = $allBroker::find()->where(['del_status' => '0'])->limit(1)->all();

$this->title = 'Discount Brokers';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('slider');?>
    <!-- ****** Categories Area Start ****** -->
        <section class="categories_area clearfix" id="about" style="padding: 30px 0 20px 0!important;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single_catagory wow fadeInUp" data-wow-delay=".3s">
                        <img src="img/catagory-img/1.jpg" alt="">
                        <div class="catagory-title">
                             <?php foreach($brokerdetail as $value){?>
                            <a target='_blank' href="/site/allbroker?type=Full service Broker">
                                <h5>Full Service Broker</h5>
                            </a>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single_catagory wow fadeInUp" data-wow-delay=".6s">
                        <img src="img/catagory-img/2.jpg" alt="">
                        <div class="catagory-title">
                            <a target='_blank' href="/site/allbroker?type=Discount Broker">
                                <h5>Discount Broker</h5>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single_catagory wow fadeInUp" data-wow-delay=".9s">
                        <img src="img/catagory-img/3.jpg" alt="">
                        <div class="catagory-title">
                            <a target='_blank' href="/site/allbroker?type=Bank Broker">
                                <h5>Bank Broker</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categories_area clearfix" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single_catagory wow fadeInUp" data-wow-delay=".3s">
                        <img src="img/catagory-img/1.jpg" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-8">
                    <div class="post-content">
                    <a href="#">
                        <h1 class="post-headline">Boil The Kettle And Make A Cup Of Tea Folks, This Is Going To Be A Big One!</h1>
                    </a>
                    <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.</p>
                    <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.</p>
                    <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.</p>
                   

                </div>
            </div>
        </div></div>
    </section>    
    <!-- ****** Categories Area End ****** -->


    <section class="clearfix" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-post wow fadeInUp" data-wow-delay=".2s">
                        <h1 class="post-headline text-center">COMPARE BROKRAGES FIRMS WITH <span style="color: #fc6c3f;">RIGHT PERAMETERS</span></h1>
                    </div>
                </div>
                <!--Table-->
                <table class="table">

                    <!--Table head-->
                    <thead class="blue-grey lighten-4">
                        <tr>
                            <th>S.no</th>
                            <th>Base of difference</th>
                            <th>Discount Broker</th>
                            <th>Full service Broker</th>
                            <th>Bank Broker</th>
                        </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Brokerage Charges</td>
                            <td>Brokerage Charges are very less as Flat Rs 20 and below on the basis of per order executed for Equity, Future &Option, Commodity and Currency.</td>
                            <td>Brokerage Charges are variable % in nature of total volume per order executed for Equity , Future & Option, Commodity and Currency </td>
                            <td>Brokerage Charges are very High as compare to Discount brokers and Full services Brokers </td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>AMC Charges</td>
                            <td>Lowest  and Zero  Charges </td>
                            <td>Basic and above Rs 5oo  Charges</td>
                            <td>Saving Bank Account Maintenance Balance with Higher AMC Charges.</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Trading Software Charges</td>
                            <td>Free of cost All trading software and tool</td>
                            <td>Some Charges applicable for Trading software and tools </td>
                            <td>Higher charges applicable trading software and tool</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>3 in one Demat Account</td>
                            <td>Technology based Online Demat account </td>
                            <td>Traditional Demat Opening Process </td>
                            <td>3 in one Demat with saving Bank account</td>
                        </tr>
                        <tr>
                            <th scope="row">5</th>
                            <td>Value Added Services </td>
                            <td>Offering Technology Based online trading and Investment services</td>
                            <td>Offering Negotiable charges , Research and advisory, Loan , Insurance  and IPO Investing services etc.</td>
                            <td>Offering  Research and Advisory , Wealth Management , HNI Services , Loan. Insurance , Portfolio Management  and IPO Investing etc.</td>
                        </tr>
                        <tr>
                            <th scope="row">6</th>
                            <td>Depository Charges (DP Charges)</td>
                            <td>Very Lowest and Basic Charges </td>
                            <td>Nominal Charges Applicable </td>
                            <td>Very Higher Charges applicable</td>
                        </tr>
                        <tr>
                            <th scope="row">7</th>
                            <td>Call & Trade Charges and Services </td>
                            <td>Flat Rs 20 and below if order executed on phone other than brokerages</td>
                            <td>Relionship Manger assign to customer  there is no call and Trade charges</td>
                            <td>Call and Trade Facility not avlaible</td>
                        </tr>
                        <tr>
                            <th scope="row">8</th>
                            <td>Transaction and turnover charges </td>
                            <td>Very lowest in every segment</td>
                            <td>Variable and Higher for every segment</td>
                            <td>Much Higher and fixed for every segment</td>
                        </tr>
                        <tr>
                            <th scope="row">9</th>
                            <td>Customer service support</td>
                            <td>Dedicated support Team</td>
                            <td>Personalized</td>
                            <td>Non personalized and banking based </td>
                        </tr>
                        <tr>
                            <th scope="row">10</th>
                            <td>Target Customer s</td>
                            <td>Day Traders and Retail investors </td>
                            <td>Trader and Investors with HNI Class</td>
                            <td>All saving Bank account Holders</td>
                        </tr>
                        <tr>
                            <th scope="row">11</th>
                            <td>Leading Broker</td>
                            <td>Zerodha</td>
                            <td>Moti Lal Oswal</td>
                            <td>ICICI direct</td>
                        </tr>
                    </tbody>
                    <!--Table body-->
                </table>
            </div>    
        </div>
    </section>
    </br>
<div class="container" style="max-width: 100%!important">
            <div class="row" id="bak">
                <div class="col-12 col-md-12 col-lg-12">
                    <h1 style="text-align: center; color: #fff; padding: 15px; font-size: 35px;">TOP Brokers Of India</h1><br/>
                    <div class="col-12 col-lg-12">
                        <div class="row no-gutters">
                            <div class="row col-md-12">
                                <?php foreach($brokerdetails as $value){?>
                                <div class="col-md-3" style="margin-bottom: 4em;">
                                    <div class="container"> 
                                    <div class="container">  
                                 <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank"><img src="<?= '/'.$value['image']; ?>" alt="" height="120" width="80%"></a> 
                                 <h3 style="color: #fff!important; padding-top: 15px;"><?= $value['broker_name']?></h3>
                                 </div></div>
                             </div>
                                <?php } ?>
                            </div>                            
                        </div>
                    </div>         
                </div>
            </div></div>
    <!-- ****** Blog Area Start ****** -->

    <section class="blog_area section_padding_0_80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="row"><br/>
                        <h1 style="text-align: center!important; font-size: 32px; padding: 20px">BLOG</h1><br/>
                        <!-- Single Post -->
                       <!--  <div class="col-12">
                            <div class="single-post wow fadeInUp" data-wow-delay=".2s"> -->
                                <!-- Post Thumb -->
                               <!--  <h1 class="post-headline">Why Choose Us!</h1><br />
                                <div class="post-thumb">
                                    <img src="img/blog-img/1.jpg" alt="">
                                </div> -->
                                <!-- Post Content -->
                                <!-- <div class="post-content">
                                    <a href="#">
                                        <h2 class="post-headline">Boil The Kettle And Make A Cup Of Tea Folks, This Is Going To Be A Big One!</h2>
                                    </a>
                                    <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat.</p>
                                    <a href="/site/about/" class="read-more">Continue Reading..</a>
                                </div>
                            </div>
                        </div> -->

                        <!-- Single Post -->
                            
                        <?php 
                        $allPosts = $postModel::find()->where(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC])->limit(3)->all();
                        foreach($allPosts as $post){
                            $postDetail = $post->postDetail;
                           
                            $commentCount = $postComment::find()->where(['post_id' => $post->id,'del_status' => '0'])->andWhere(['<>','approved_by','NULL'])->count();
                                $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
                            ?>
                        <div class="col-12">
                            <div class="list-blog single-post d-sm-flex wow fadeInUpBig" data-wow-delay=".2s">
                                <!-- Post Thumb -->
                                <div class="post-thumb">
                                   <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="<?= $postImage->url;?>"  style="width: 450px; border-radius: 30px!important;"></a>
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-meta d-flex">
                                    <?php $mubUser = $mubUserModel::findOne($post->mub_user_id); ?>
                                        <div class="post-author-date-area d-flex">
                                            <!-- Post Author -->
                                            <div class="post-author">
                                                <a href="#"><?= $mubUser->username;?> </a>
                                            </div>
                                            <!-- Post Date -->
                                            <div class="post-date">
                                                <a href="#"><?= $post->created_at;?></a>
                                            </div>
                                        </div>
                                        <!-- Post Comment & Share Area -->
                                        <div class="post-comment-share-area d-flex">
                                            <!-- Post Favourite -->
                                            <div class="post-favourite">
                                                <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> <?= $postDetail->read_count;?></a>
                                            </div>
                                            <!-- Post Comments -->
                                            <div class="post-comments">
                                                <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> <?= ($commentCount == 0) ? 'No' : $commentCount;?></a>
                                            </div>
                                            <!-- Post Share -->
                                           
                                        </div>
                                    </div>
                                    <a href="#">
                                        <a href="<?= '/blog/post-detail?id='.$post->url;?>"><h4 class="post-headline"><?=$post->post_title;?></h4></a>
                                    </a>
                                    <p><?= $post->post_excerpt;?></p>
                                    <a href="<?= '/blog/post-detail?id='.$post->url;?>" class="read-more">Continue Reading..</a>
                                </div>
                            </div>
                        </div>

                     <?php }?>

                      
                    </div>
                </div>

                <!-- ****** Blog Sidebar ****** -->
                <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                    <div class="blog-sidebar mt-5 mt-lg-0"><br/><br/><br/><br/>
                        <div class="single-widget-area popular-post-widget">
                            <div class="widget-title text-center">
                                <h6>Populer Post</h6>
                            </div><br/>
                         <?php 
                        $allPosts = $postModel::find()->where(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC])->limit(7)->all();
                        foreach($allPosts as $post){
                            $postDetail = $post->postDetail;
                           
                            $commentCount = $postComment::find()->where(['post_id' => $post->id,'del_status' => '0'])->andWhere(['<>','approved_by','NULL'])->count();
                                $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
                            ?>
                        <div class="single-populer-post d-flex">
                            <a href="<?= '/blog/post-detail?id='.$post->url;?>"><img src="<?= $postImage->url;?>" style="width: 130px;"></a>
                            <div class="post-content">
                                <a href="<?= '/blog/post-detail?id='.$post->url;?>">
                                    <h5><?=$post->post_title;?></h5>
                                </a>
                                <p><?= $mubUser->username;?> </p>
                                <p><?= $post->created_at;?></p>

                            </div>
                        </div>
                          <?php }?>
                           <!-- <div class="single-widget-area newsletter-widget">
                            <div class="widget-title text-center">
                                <h6>Newsletter</h6>
                            </div>
                            <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.</p>
                            <div class="newsletter-form">
                                <form action="#" method="post">
                                    <input type="email" name="newsletter-email" id="email" placeholder="Your email">
                                    <button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div> -->
                    </div> 
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!-- ****** Blog Area End ****** -->

    <!-- ****** Instagram Area Start ****** -->
