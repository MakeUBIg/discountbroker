
<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\loginForm;
use yii\helpers\ArrayHelper;

$this->title = 'Compare Brokers';
$this->params['breadcrumbs'][] = $this->title;
$allBroker = new app\modules\MubAdmin\modules\broker\models\Brokers();
$urlData = Yii::$app->getRequest()->getQueryParam('type');

$brokerdetails = $allBroker::find()->where(['del_status' => '0','type_of_broker' => $urlData,])->all();


?>
<div class="breadcumb-area" style="background-image: url(/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="bradcumb-title text-center">
                        <h2><?= $urlData;?></h2>
                    </div>
                </div>
            </div>
        </div>
</div>
    <div class="breadcumb-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Brokers</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ****** Single Blog Area Start ****** -->
    <section class="single_blog_area section_padding_80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-12">
                    <div class="row no-gutters">
                        <div class="row col-md-12">
                            <?php foreach($brokerdetails as $value){?>
                            <div class="col-md-3" style="margin-bottom: 6em;">
                             <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank"><img src="<?= '/'.$value['image']; ?>" alt="" height="160" width="100%"></a> 
                             </div>
                            <?php } ?>
                        </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
