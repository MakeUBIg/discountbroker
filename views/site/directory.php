<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use  \app\modules\MubAdmin\modules\broker\models\Brokers;

$this->title = 'Broker Directory';
$this->params['breadcrumbs'][] = $this->title;
$brokers = new Brokers();
$brokerage = new \app\modules\MubAdmin\modules\broker\models\Brokerage();
$transaction = new \app\modules\MubAdmin\modules\broker\models\TransactionCharge();
$platforms = new \app\modules\MubAdmin\modules\broker\models\Platforms();
$support = new \app\modules\MubAdmin\modules\broker\models\CustomersSupport();

$allbrokers = $brokers::find()->where(['del_status' => '0'])->orderBy(['id'=>SORT_DESC])->all();
$allbrokerage = $brokerage::find()->where(['del_status' => '0'])->all();
$alltransaction = $transaction::find()->where(['del_status' => '0'])->orderBy(['id'=>SORT_DESC])->all();
$allplatforms = $platforms::find()->where(['del_status' => '0'])->all();
$allsupport = $support::find()->where(['del_status' => '0'])->orderBy(['id'=>SORT_DESC])->all();

?>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style type="text/css">.w3-black, .w3-hover-black:hover {
    color: #fff!important;
    background-color: #9c9696!important;
}
.w3-bar .w3-bar-item {
    padding: 13px 16px!important;
    padding-right: 60px!important;
    padding-left: 66px!important;}
    .w3-border {
    padding: 10px;}
</style>
<div class="breadcumb-area" style="background-image: url(/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="bradcumb-title text-center">
                        <h2>Broker Directory</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcumb-nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Broker Directory</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="w3-container">
    <div class="container">
    <h3>SELECT THE BEST BROKER IN INDIA</h3><br/>
    </div>
    <div class="container">
 <div class="w3-container">
  <div class="w3-bar w3-black">
    <button class="w3-bar-item w3-button tablink w3-red" onclick="openCity(event,'London')">BROKERAGE</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Paris')">TRANSACTION &amp; OTHER COSTS</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Tokyo')">PLATFORMS</button>
    <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'Tokyo')">SUPPORT &amp; TOOLS</button>
  </div>
  <br/>
  <div id="London" class="w3-container w3-border city">
    <?php foreach($allbrokerage as $value){
        $broker = Brokers::findOne($value->broker_id);
        $imagePath = '/'.$broker->image;
        $name = $broker->broker_name;
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 item">
            <div class="thumbnail">  
              <div class="caption">
                <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank">
                  <div class="d-cb-dir-gbox-h" style="height:159px;background:url(<?=$imagePath?>) no-repeat ;background-size:contain"> <!--<img src="http://www.brokersanalysis.com/assets/images/gallery/thumbs/Zerodha_logo.svg_.png" width="100%"/>-->
                    <!-- <div class="d-cb-glass-fx"><h5><?=$name;?></h5></div> -->
                  </div>
                  </a>
                <div class="d-cb-dir-gbox">
                 <!--  <div class="cb-d-tabheader">
                    <p>
                    <a class="lead_generation fancybox" data-toggle="modal" data-target="#lead-generation" data-company_name="SASOnline " href="#inline1" onclick="showUser(27);"><span>Best Deal</span> </a><a class="fancybox" href="#inline1" onclick="showUser(27);">Open an Account <i class="fa fa-arrow-circle-o-right"></i></a></p>
                  </div> -->

                  <div class="d-cb-dir-gbox-b">
                    <ul class="d-cb-dir-gbox-ul1" style="margin-bottom: 0px;">
                     
                       <li><i class="fa fa-list-alt"></i><span>Equity:</span> <?= $value['equity']; ?></li>
                      <li><i class="fa fa-list-alt"></i><span>Equity Futures:</span> <?= $value['equity_futures']; ?></li>
                      <li><i class="fa fa-list-ul"></i><span>Equity Options:</span> <?= $value['equity_options']; ?></li>
                      <li><i class="fa fa-rupee"></i><span>Currency Futures:</span> <?= $value['currency_futures']; ?></li>
                      <li><i class="fa fa-rupee"></i><span>Currency Options:</span> <?= $value['currency_options']; ?></li>
                      <li><i class="fa fa-suitcase"></i><span>Commodity:</span> <?= $value['commodity']; ?></li>
                    </ul>
                    </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div><?php }?>
  </div>

  <div id="Paris" class="w3-container w3-border city" style="display:none">
     <?php foreach($alltransaction as $value){
        $broker = Brokers::findOne($value->broker_id);
        $imagePath = '/'.$broker->image;
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 item">
            <div class="thumbnail">  
              <div class="caption">
                <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank">
                  <div class="d-cb-dir-gbox-h" style="height:159px;background:url(<?=$imagePath?>) no-repeat ;background-size:contain"> <!--<img src="http://www.brokersanalysis.com/assets/images/gallery/thumbs/Zerodha_logo.svg_.png" width="100%"/>-->
                    <div class="d-cb-glass-fx"> <span id="broknamedy27"> </span> </div>
                  </div>
                  </a>
                <div class="d-cb-dir-gbox">

                  <div class="d-cb-dir-gbox-b">
                    <ul class="d-cb-dir-gbox-ul1" style="margin-bottom: 0px;">
                     
                       <li><i class="fa fa-list-alt"></i><span>Equity Delivery:</span> <?= $value['equity_delivery']; ?></li>
                      <li><i class="fa fa-list-alt"></i><span>Equity Intraday:</span> <?= $value['equity_intraday']; ?></li>
                      <li><i class="fa fa-list-ul"></i><span>Equity Futures:</span> <?= $value['equity_futures']; ?></li>
                      <li><i class="fa fa-rupee"></i><span>Equity Options:</span> <?= $value['equity_options']; ?></li>
                      <li><i class="fa fa-rupee"></i><span>Currency Futures:</span> <?= $value['currency_futures']; ?></li>
                      <li><i class="fa fa-suitcase"></i><span>Currency Options:</span> <?= $value['currency_options']; ?></li>
                       <li><i class="fa fa-suitcase"></i><span>Commodity:</span> <?= $value['commodity']; ?></li>

                    </ul>
                    </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div><?php }?>
  </div>

  <div id="Tokyo" class="w3-container w3-border city" style="display:none">
     <?php foreach($allplatforms as $value){
        $broker = Brokers::findOne($value->broker_id);
        $imagePath = '/'.$broker->image;
        ?>
        <div class="col-md-3 col-sm-6 col-xs-12 item">
            <div class="thumbnail">  
              <div class="caption">
                <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank">
                  <div class="d-cb-dir-gbox-h" style="height:159px;background:url(<?=$imagePath?>) no-repeat ;background-size:contain"> <!--<img src="http://www.brokersanalysis.com/assets/images/gallery/thumbs/Zerodha_logo.svg_.png" width="100%"/>-->
                    <div class="d-cb-glass-fx"> <span id="broknamedy27"> </span> </div>
                  </div>
                  </a>
                <div class="d-cb-dir-gbox">

                  <div class="d-cb-dir-gbox-b">
                    <ul class="d-cb-dir-gbox-ul1" style="margin-bottom: 0px;">
                     
                       <li><i class="fa fa-suitcase"></i><span>Software:</span><?php if($value['software'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                         else {?>
                         <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['software']; ?></li>
                         <li><i class="fa fa-suitcase"></i><span>Web/HTML 5:</span><?php if($value['web_html'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                         else {?>
                         <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['web_html']; ?></li>
                         <li><i class="fa fa-suitcase"></i><span>Mobile:</span><?php if($value['mobile'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                         else {?>
                         <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['mobile']; ?></li>
                         <li><i class="fa fa-suitcase"></i><span>Trading Platform:</span> <?= $value['trading_platform']; ?></li>
                      

                    </ul>
                    </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div><?php }?>
  </div>
  </div>
  <div id="India" class="w3-container w3-border city" style="display:none">
      <?php foreach($allsupport as $value){
          $broker = Brokers::findOne($value->broker_id);
          $imagePath = '/'.$broker->image;
          ?>
          <div class="col-md-3 col-sm-6 col-xs-12 item">
              <div class="thumbnail">  
                <div class="caption">
                  <a href="/site/brokerdetails?id=<?= $value['id']; ?>" target="_blank">
                    <div class="d-cb-dir-gbox-h" style="height:159px;background:url(<?=$imagePath?>) no-repeat ;background-size:contain"> <!--<img src="http://www.brokersanalysis.com/assets/images/gallery/thumbs/Zerodha_logo.svg_.png" width="100%"/>-->
                      <div class="d-cb-glass-fx"> <span id="broknamedy27"> </span> </div>
                    </div>
                    </a>
                  <div class="d-cb-dir-gbox">

                    <div class="d-cb-dir-gbox-b">
                      <ul class="d-cb-dir-gbox-ul1" style="margin-bottom: 0px;">
                       
                         <li><i class="fa fa-suitcase"></i><span>24/7 Customer Service:</span><?php if($value['customer_service'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['customer_service']; ?></li>
                           <li><i class="fa fa-suitcase"></i><span>Email Support</span><?php if($value['email_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['email_support']; ?></li>
                           <li><i class="fa fa-suitcase"></i><span>Online Live Chat </span><?php if($value['online_live_chat'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['online_live_chat']; ?></li>

                            <li><i class="fa fa-suitcase"></i><span>Phone Support:</span><?php if($value['phone_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['phone_support']; ?></li>

                            <li><i class="fa fa-suitcase"></i><span>Toll Free Number</span><?php if($value['toll_free_number'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['toll_free_number']; ?></li>
                            <li><i class="fa fa-suitcase"></i><span>Through Branches:</span><?php if($value['through_branches'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
                           else {?>
                           <i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $value['through_branches']; ?></li>
                          
                        

                      </ul>
                      </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div><?php }?>
    </div>
  </div>

<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>
