<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\loginForm;
use yii\helpers\ArrayHelper;

$this->title = 'Compare Brokers';
$this->params['breadcrumbs'][] = $this->title;
$allBrokers = $brokers->getAll('broker_name');
if(isset($brokersData))
{
	if(isset($brokersData['first']))
	{
		$first = $brokersData['first'];
	}
	if(isset($brokersData['second'])){
		
		$second = $brokersData['second'];
	}
	if (isset($brokersData['third'])) {

		$third = $brokersData['third'];
	}
	
	
}
?>
<style type="text/css">
.hei53{
height: 53px!important;
}
.hei197{
height: 197px!important;
}
.hei61{
height: 80px!important;
}
@media only screen and (max-width: 540px) {
.hei80{
height: 80px!important;
font-weight: 400!important;
padding: 0px!important;
}
.hei21{
	padding: 10px!important;

}
.hei20{
height: 120px!important;
padding: 0px!important;
font-weight: 400!important;
}
.hei50{
height: 50px!important;
padding: 0px!important;
font-size: 12px!important;
font-weight: 400!important;
}
.hei55{
height: 70px!important;
padding: 0px!important;
font-weight: 400!important;
}
.hei56{
height: 90px!important;
padding: 0px!important;
font-weight: 400!important;
}
.hei64{
height: 64px!important;
padding: 0px!important;
font-weight: 400!important;
}
.hei59{
height: 59px!important;
padding: 0px!important;
font-weight: 400!important;
}
.hei197{
height: 197px!important;
font-size: 11px!important;
padding: 0px!important;
font-weight: 400!important;
}
}
</style>
<section class="cd-products-comparison-table container">
		
		<div class="container full_search">

			<?php 
				$form = ActiveForm::begin([
				'method' => 'get',
    			'action' => ['compare/entry'],
		    	'id' => 'login-form',
		   		'options' => ['class' => ''],
				])
			?>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<div class="form-group">
					  	<?php 
					  	echo $form->field($brokers,'broker_name')->dropdownList($allBrokers,
							['name'=>'broker_first','prompt'=>'Select a Broker','id'=>'first_broker']
						)->label(false);

					    ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  	<?php 
					  	echo $form->field($brokers,'broker_name')->dropdownList($allBrokers,
							['name'=>'broker_second','prompt'=>'Select a Broker','id'=>'second_broker']
						)->label(false);

					    ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					  	<?php 

					  	echo $form->field($brokers,'broker_name')->dropdownList($allBrokers,
							['name'=>'broker_third','prompt'=>'Select a Broker','id'=>'third_broker']
						)->label(false);

					    ?>
					</div>
				</div>
				<div class="col-md-2">
					<?= Html::submitButton('Compare Brokers', ['class' => 'btn btn-info','id'=>'broker_check']) ?>
				</div>	
			</div>
			<?php ActiveForm::end() ?>
      	</div>
      	<?php if(!empty($brokersData)) {?><br/>
      	<header>
			<h2><i class="fa fa-star" aria-hidden="true"></i> Background</h2>
		</header>
		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Incorporation’s year</li>
					<li class="hei80">Type of Broker</li>
					<li class="hei80">Account Type</li>
					<li class="hei80">Supported Exchanges</li>
					<li class="hei80">Member of (NSDL/CDSL)</li>
				</ul>
			</div> <!-- .features -->
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					
					<?php if(isset($first)){ ?> 
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">	
							<h3>
								<?= $brokersData['first']['broker']['broker_name']; ?>
							</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['first']['broker']['incorporation_year']; ?></li>
							<li class="hei80"><?= $brokersData['first']['broker']['type_of_broker']; ?></li>
							<li class="hei80"><?= $brokersData['first']['broker']['account_type']; ?></li>
	
							<li class="hei80"><?= $brokersData['first']['broker']['supported_exchanges']; ?></li>
							<li class="hei80"><?= $brokersData['first']['broker']['member_of']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($second)){ ?> 
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">	
							<h3>
								<?= $brokersData['second']['broker']['broker_name']; ?>
							</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['second']['broker']['incorporation_year']; ?></li>
							<li class="hei80"><?= $brokersData['second']['broker']['type_of_broker']; ?></li>
							<li class="hei80"><?= $brokersData['second']['broker']['account_type']; ?></li>
	
							<li class="hei80"><?= $brokersData['second']['broker']['supported_exchanges']; ?></li>
							<li class="hei80"><?= $brokersData['second']['broker']['member_of']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">	
							<h3>
								<?= $brokersData['third']['broker']['broker_name']; ?>
							</h3>
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['third']['broker']['incorporation_year']; ?></li>
							<li class="hei80"><?= $brokersData['third']['broker']['type_of_broker']; ?></li>
							<li class="hei80"><?= $brokersData['third']['broker']['account_type']; ?></li>
	
							<li class="hei80"><?= $brokersData['third']['broker']['supported_exchanges']; ?></li>
							<li class="hei80"><?= $brokersData['third']['broker']['member_of']; ?></li>
						</ul>
					</li><!-- .product -->
					<?php } ?>
				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
		</div> <!-- .cd-products-table -->
		<?php }?>
	</section> <!-- .cd-products-comparison-table -->
	<?php if(!empty($brokersData)){?>
	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-chain-broken" aria-hidden="true"></i> Brokerage</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="he73 hei20">Equity</li>
					<li class="he73 hei20">Equity Futures</li>
					<li class="he73 hei20">Equity Options</li>
					<li class="he73 hei20">Currency Futures</li>
					<li class="he73 hei20">Currency Options</li>
					<li class="he73 hei20">Commodity</li>
					<li class="he73 hei20">Other Charges</li>

				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>	
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['equity'] ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['equity_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['equity_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['currency_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['currency_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['commodity']; ?></li>
							<li class="he73 hei20"><?= $brokersData['first']['brokerages']['other_charges']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['equity']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['equity_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['equity_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['currency_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['currency_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['commodity']; ?></li>
							<li class="he73 hei20"><?= $brokersData['second']['brokerages']['other_charges']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['equity']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['equity_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['equity_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['currency_futures']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['currency_options']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['commodity']; ?></li>
							<li class="he73 hei20"><?= $brokersData['third']['brokerages']['other_charges']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
			
				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
		</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-american-sign-language-interpreting" aria-hidden="true"></i> Stozk Broker Fees / Charges</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li>Trading only</li>
					<li class="hei50">Trading & Demat</li>
					<li class="hei50">Demat Account AMC </li>
					<li class="hei50">Commodity</li>
					<li class="hei53 hei55">DP Transaction Charge</li>
					<li class="hei53 hei50">DP Transaction Charge</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['first']['stockBrokersFees']['trading_only']; ?></li>
							<li class="hei50"><?= $brokersData['first']['stockBrokersFees']['trading_Demat']; ?></li>
							<li class="hei50"><?= $brokersData['first']['stockBrokersFees']['demat_account_amc']; ?></li>
							<li class="hei50"><?= $brokersData['first']['stockBrokersFees']['commodity']; ?></li>
							<li class="hei53 hei55"><?= $brokersData['first']['stockBrokersFees']['dp_transaction']; ?></li>
							<li class="hei50"><?= $brokersData['first']['stockBrokersFees']['offline_order_placing']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['second']['stockBrokersFees']['trading_only']; ?></li>
							<li class="hei50"><?= $brokersData['second']['stockBrokersFees']['trading_Demat']; ?></li>
							<li class="hei50"><?= $brokersData['second']['stockBrokersFees']['demat_account_amc']; ?></li>
							<li class="hei50"><?= $brokersData['second']['stockBrokersFees']['commodity']; ?></li>
							<li class="hei53 hei55"><?= $brokersData['second']['stockBrokersFees']['dp_transaction']; ?></li>
							<li class="hei50"><?= $brokersData['second']['stockBrokersFees']['offline_order_placing']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?= $brokersData['third']['stockBrokersFees']['trading_only']; ?></li>
							<li class="hei50"><?= $brokersData['third']['stockBrokersFees']['trading_Demat']; ?></li>
							<li class="hei50"><?= $brokersData['third']['stockBrokersFees']['demat_account_amc']; ?></li>
							<li class="hei50"><?= $brokersData['third']['stockBrokersFees']['commodity']; ?></li>
							<li class="hei53 hei55"><?= $brokersData['third']['stockBrokersFees']['dp_transaction']; ?></li>
							<li class="hei50"><?= $brokersData['third']['stockBrokersFees']['offline_order_placing']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->


				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			
		</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-percent" aria-hidden="true"></i> Taxes</h2>
	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei197">Securities Transaction Tax</li>
					<li class="hei59">Exchange Transaction</li>
					<li class="hei59">SEBI Charges</li>
					<li class="he53 hei64">Goods and Services Tax</li>
					<li class="hei61 hei55">Stamp Duty</li>
					
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei197"><?= $brokersData['first']['taxes']['securities_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['first']['taxes']['exchange_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['first']['taxes']['sebi_charges']; ?></li>
							<li class="hei64"><?= $brokersData['first']['taxes']['goods_services']; ?></li>
							<li class="hei61 hei55"><?= $brokersData['first']['taxes']['stamp_duty']; ?></li>
							
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei197"><?= $brokersData['second']['taxes']['securities_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['second']['taxes']['exchange_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['second']['taxes']['sebi_charges']; ?></li>
							<li class="hei64"><?= $brokersData['second']['taxes']['goods_services']; ?></li>
							<li class="hei61 hei55"><?= $brokersData['second']['taxes']['stamp_duty']; ?></li>
							
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei197"><?= $brokersData['third']['taxes']['securities_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['third']['taxes']['exchange_transaction']; ?></li>
							<li class="hei59"><?= $brokersData['third']['taxes']['sebi_charges']; ?></li>
							<li class="hei64"><?= $brokersData['third']['taxes']['goods_services']; ?></li>
							<li class="hei61 hei55"><?= $brokersData['third']['taxes']['stamp_duty']; ?></li>
							
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
		<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-money" aria-hidden="true"></i> Transaction Charges</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei61">Equity Delivery</li>
					<li class="hei61">Equity Intraday</li>
					<li class="hei61">Equity Futures</li>
					<li class="hei61">Equity Options </li>
					<li class="hei61">Currency Futures </li>
					<li class="hei61">Currency Options</li>
					<li class="hei61">Commodity</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['equity_delivery']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['equity_intraday']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['equity_futures']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['equity_options']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['currency_futures']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['currency_options']; ?></li>
							<li class="hei61"><?= $brokersData['first']['transactionCharges']['commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>
					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['equity_delivery']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['equity_intraday']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['equity_futures']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['equity_options']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['currency_futures']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['currency_options']; ?></li>
							<li class="hei61"><?= $brokersData['second']['transactionCharges']['commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['equity_delivery']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['equity_intraday']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['equity_futures']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['equity_options']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['currency_futures']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['currency_options']; ?></li>
							<li class="hei61"><?= $brokersData['third']['transactionCharges']['commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-balance-scale" aria-hidden="true"></i> Margins</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="he73 hei56">Equity Delivery</li>
					<li class="hei55">Equity Futures</li>
					<li class="he53 hei55">Equity Options </li>
					<li class="hei55">Currency Futures </li>
					<li class="he53 hei55">Currency Options</li>
					<li class="hei55">Commodity</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei56"><?= $brokersData['first']['margins']['m_equity_delivery']; ?></li>
							<li class="hei55"><?= $brokersData['first']['margins']['m_equity_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['first']['margins']['m_equity_options']; ?></li>
							<li class="hei55"><?= $brokersData['first']['margins']['m_currency_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['first']['margins']['m_currency_options']; ?></li>
							<li class="hei55"><?= $brokersData['first']['margins']['m_commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> 
					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei56"><?= $brokersData['second']['margins']['m_equity_delivery']; ?></li>
							<li class="hei55"><?= $brokersData['second']['margins']['m_equity_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['second']['margins']['m_equity_options']; ?></li>
							<li class="hei55"><?= $brokersData['second']['margins']['m_currency_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['second']['margins']['m_currency_options']; ?></li>
							<li class="hei55"><?= $brokersData['second']['margins']['m_commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?>  <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="he73 hei56"><?= $brokersData['third']['margins']['m_equity_delivery']; ?></li>
							<li class="hei55"><?= $brokersData['third']['margins']['m_equity_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['third']['margins']['m_equity_options']; ?></li>
							<li class="hei55"><?= $brokersData['third']['margins']['m_currency_features']; ?></li>
							<li class="he53 hei55"><?= $brokersData['third']['margins']['m_currency_options']; ?></li>
							<li class="hei55"><?= $brokersData['third']['margins']['m_commodity']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-comments-o" aria-hidden="true"></i> Platforms</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59">Software</li>
					<li class="hei59">Web/HTML 5</li>
					<li class="hei59">Mobile</li>
					<li class="he53 hei59">Trading Platform</li>

				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['first']['platforms']['software'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['platforms']['software']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['platforms']['web_html'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['platforms']['web_html']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['platforms']['mobile'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['platforms']['mobile']; ?></li>
							<li class="he53 hei59"><?= $brokersData['first']['platforms']['trading_platform']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['second']['platforms']['software'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['platforms']['software']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['platforms']['web_html'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['platforms']['web_html']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['platforms']['mobile'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['platforms']['mobile']; ?></li>
							<li class="hei59"><?= $brokersData['second']['platforms']['trading_platform']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['third']['platforms']['software'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['platforms']['software']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['platforms']['web_html'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['platforms']['web_html']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['platforms']['mobile'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['platforms']['mobile']; ?></li>
							<li class="hei59"><?= $brokersData['third']['platforms']['trading_platform']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-bar-chart" aria-hidden="true"></i> Charting</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59 he53">Intraday</li>
					<li class="he53 hei59">End of Day</li>
					<li class="he53 hei59">Coding/Backtesting</li>

				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59 he53"><?= $brokersData['first']['chartings']['intraday']; ?></li>
							<li class="he53 hei59"><?= $brokersData['first']['chartings']['end_of_day']; ?></li>
							<li class="he53 hei59"><?= $brokersData['first']['chartings']['coding_backtesting']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59 he53"><?= $brokersData['second']['chartings']['intraday']; ?></li>
							<li class="he53 hei59"><?= $brokersData['second']['chartings']['end_of_day']; ?></li>
							<li class="he53 hei59"><?= $brokersData['second']['chartings']['coding_backtesting']; ?></li>
							</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59 he53"><?= $brokersData['third']['chartings']['intraday']; ?></li>
							<li class="he53 hei59"><?= $brokersData['third']['chartings']['end_of_day']; ?></li>
							<li class="he53 hei59"><?= $brokersData['third']['chartings']['coding_backtesting']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-tasks" aria-hidden="true"></i> Brokerages plan</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei50">Plan 1</li>
					<li class="hei50">Plan 2</li>
					<li class="hei50">Plan 3</li>
					
				</ul>
			</div> <!-- .features -->

			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei50"><?= $brokersData['first']['plans']['plan_1']; ?></li>
							<li class="hei50"><?= $brokersData['first']['plans']['plan_2']; ?></li>
							<li class="hei50"><?= $brokersData['first']['plans']['plan_3']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei50"><?= $brokersData['second']['plans']['plan_1']; ?></li>
							<li class="hei50"><?= $brokersData['second']['plans']['plan_2']; ?></li>
							<li class="hei50"><?= $brokersData['second']['plans']['plan_3']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?><!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei50"><?= $brokersData['third']['plans']['plan_1']; ?></li>
							<li class="hei50"><?= $brokersData['third']['plans']['plan_2']; ?></li>
							<li class="hei50"><?= $brokersData['third']['plans']['plan_3']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-users" aria-hidden="true"></i> Reporting</h2>

	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59">Online Trade Reports</li>
					<li class="hei59">Online PNL Reports</li>
					<li class="hei59">Online Contract Notes</li>
					<li class="hei59">Daily Market Report</li>

			    </ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?php if($brokersData['first']['reportings']['online_trade'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['reportings']['online_trade']; ?></li>
							<li><?php if($brokersData['first']['reportings']['online_pnl'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['reportings']['online_pnl']; ?></li>
							<li><?php if($brokersData['first']['reportings']['online_contract'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['reportings']['online_contract']; ?></li>
							<li><?php if($brokersData['first']['reportings']['daily_market'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['reportings']['daily_market']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?php if($brokersData['second']['reportings']['online_trade'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['reportings']['online_trade']; ?></li>
							<li><?php if($brokersData['second']['reportings']['online_pnl'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['reportings']['online_pnl']; ?></li>
							<li><?php if($brokersData['second']['reportings']['online_contract'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['reportings']['online_contract']; ?></li>
							<li><?php if($brokersData['second']['reportings']['daily_market'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['reportings']['daily_market']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li><?php if($brokersData['third']['reportings']['online_trade'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['reportings']['online_trade']; ?></li>
							<li><?php if($brokersData['third']['reportings']['online_pnl'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['reportings']['online_pnl']; ?></li>
							<li><?php if($brokersData['third']['reportings']['online_contract'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['reportings']['online_contract']; ?></li>
							<li><?php if($brokersData['third']['reportings']['daily_market'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['reportings']['daily_market']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-book" aria-hidden="true"></i> Feature/ Convenience/ Support & Tools</h2>
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59">Good Till Cancelled (GTC) </li>
					<li class="hei59">Online Demo</li>
					<li class="hei59">Online Portfolio</li>
					<li class="hei59">Instant Fund withdrawal</li>
					<li class="hei59">Relationship Managers</li>
					<li class="hei59">Research & Tips</li>
					<li class="hei59">Brokerage Calculator</li>
					<li class="hei59">Margin Calculator</li>
					<li class="hei59">Bracket orders & Trailing</li>
					<li class="hei59">Cover order</li>
					<li class="hei59">Training & Education</li>
					<li class="hei59">3 in 1 Account</li>
					<li class="hei59">Mobile Trading</li>
					<li class="hei59">Trading Platform</li>
					<li class="hei59">Intraday Square-off Time</li>
					<li class="hei59">Free Tips</li>
					<li class="hei59">News Alerts </li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['good_till_cancelled'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['good_till_cancelled']; ?>
							</li>

							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['online_demo'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['online_demo']; ?></li>

							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['online_portfolio'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['online_portfolio']; ?></li>

							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['instant_fund_withdrawal'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['instant_fund_withdrawal']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['relationship_managers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['relationship_managers']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['research_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['research_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['brokerage_calculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['brokerage_calculator']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['margin_alculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['margin_alculator']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['bracket_orders'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['bracket_orders']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['cover_order'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['cover_order']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['training_sducation'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['training_sducation']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['in_account'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['in_account']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['mobile_trading'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['mobile_trading']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['trading_platform'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['trading_platform']; ?></li>
							<li class="hei59"><?= $brokersData['first']['featureSupportTools']['intraday_square']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['free_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['free_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['featureSupportTools']['news_alerts'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['featureSupportTools']['news_alerts']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['good_till_cancelled'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['good_till_cancelled']; ?>
							</li>

							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['online_demo'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['online_demo']; ?></li>

							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['online_portfolio'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['online_portfolio']; ?></li>

							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['instant_fund_withdrawal'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['instant_fund_withdrawal']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['relationship_managers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['relationship_managers']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['research_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['research_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['brokerage_calculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['brokerage_calculator']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['margin_alculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['margin_alculator']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['bracket_orders'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['bracket_orders']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['cover_order'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['cover_order']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['training_sducation'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['training_sducation']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['in_account'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['in_account']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['mobile_trading'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['mobile_trading']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['trading_platform'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['trading_platform']; ?></li>
							<li class="hei59"><?= $brokersData['second']['featureSupportTools']['intraday_square']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['free_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['free_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['featureSupportTools']['news_alerts'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['featureSupportTools']['news_alerts']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['good_till_cancelled'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['good_till_cancelled']; ?>
							</li>

							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['online_demo'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['online_demo']; ?></li>

							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['online_portfolio'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['online_portfolio']; ?></li>

							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['instant_fund_withdrawal'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['instant_fund_withdrawal']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['relationship_managers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['relationship_managers']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['research_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['research_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['brokerage_calculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['brokerage_calculator']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['margin_alculator'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['margin_alculator']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['bracket_orders'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['bracket_orders']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['cover_order'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['cover_order']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['training_sducation'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['training_sducation']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['in_account'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['in_account']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['mobile_trading'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['mobile_trading']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['trading_platform'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['trading_platform']; ?></li>
							<li class="hei59"><?= $brokersData['third']['featureSupportTools']['intraday_square']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['free_tips'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['free_tips']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['featureSupportTools']['news_alerts'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['featureSupportTools']['news_alerts']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->	
<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-universal-access" aria-hidden="true"></i> Investment Options</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59">Stock / Equity</li>
					<li class="hei59">Commodity</li>
					<li class="hei59">Currency</li>
					<li class="hei59">Initial Public Offers (IPO)</li>
					<li class="hei59">Mutual Funds</li>
					<li class="hei59">Bond / NCD</li>
					<li class="hei59">Debt</li>
					<li class="hei53 hei59">Investor category</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['first']['investments']['stock_equity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['stock_equity']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['first']['investments']['commodity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['commodity']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['investments']['currency'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['currency']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['investments']['initial_public_offers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['initial_public_offers']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['investments']['mutual_funds'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['mutual_funds']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['investments']['bond_ncd'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['bond_ncd']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['investments']['debt'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['investments']['debt']; ?></li>
							<li class="hei53 hei59"><?= $brokersData['first']['investments']['investor_category']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['second']['investments']['stock_equity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['stock_equity']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['second']['investments']['commodity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['commodity']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['investments']['currency'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['currency']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['investments']['initial_public_offers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['initial_public_offers']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['investments']['mutual_funds'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['mutual_funds']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['investments']['bond_ncd'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['bond_ncd']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['investments']['debt'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['investments']['debt']; ?></li>
							<li class="hei53 hei59"><?= $brokersData['second']['investments']['investor_category']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['third']['investments']['stock_equity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['stock_equity']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['third']['investments']['commodity'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['commodity']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['investments']['currency'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['currency']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['investments']['initial_public_offers'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['initial_public_offers']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['investments']['mutual_funds'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['mutual_funds']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['investments']['bond_ncd'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['bond_ncd']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['investments']['debt'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['investments']['debt']; ?></li>
							<li class="hei53 hei59"><?= $brokersData['third']['investments']['investor_category']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<section class="cd-products-comparison-table container">
		<header>
			<h2><i class="fa fa-phone" aria-hidden="true"></i> Customer Support</h2>	
		</header>

		<div class="cd-products-table">
			<div class="features">
				<div class="top-info">Discount Brokers Compare</div>
				<ul class="cd-features-list">
					<li class="hei59">24/7 Customer Service</li>
					<li class="hei59">Email Support</li>
					<li class="hei59">Online Live Chat </li>
					<li class="hei59">Phone Support</li>
					<li class="hei59">Toll Free Number</li>
					<li class="hei59">Through Branches</li>
				</ul>
			</div> <!-- .features -->
			
			<div class="cd-products-wrapper">
				<ul class="cd-products-columns">
					<?php if(isset($first)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['first']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['customer_service'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?>
							<?= $brokersData['first']['customersSupports']['customer_service']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['email_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['customersSupports']['email_support']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['online_live_chat'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['customersSupports']['online_live_chat']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['phone_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['customersSupports']['phone_support']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['toll_free_number'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['customersSupports']['toll_free_number']; ?></li>
							<li class="hei59"><?php if($brokersData['first']['customersSupports']['through_branches'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['first']['customersSupports']['through_branches']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($second)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['second']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['customer_service'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?>
							<?= $brokersData['second']['customersSupports']['customer_service']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['email_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['customersSupports']['email_support']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['online_live_chat'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['customersSupports']['online_live_chat']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['phone_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['customersSupports']['phone_support']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['toll_free_number'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['customersSupports']['toll_free_number']; ?></li>
							<li class="hei59"><?php if($brokersData['second']['customersSupports']['through_branches'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['second']['customersSupports']['through_branches']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

					<?php if(isset($third)){?>
					<li class="product">
						<div class="top-info">
							<img src="/<?= $brokersData['third']['broker']['image']; ?>" alt="product image">
						</div> <!-- .top-info -->

						<ul class="cd-features-list">
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['customer_service'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?>
							<?= $brokersData['third']['customersSupports']['customer_service']; ?>
							</li>
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['email_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['customersSupports']['email_support']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['online_live_chat'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['customersSupports']['online_live_chat']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['phone_support'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['customersSupports']['phone_support']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['toll_free_number'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['customersSupports']['toll_free_number']; ?></li>
							<li class="hei59"><?php if($brokersData['third']['customersSupports']['through_branches'] == "unavailable"){?><i class="fa fa-window-close colr2" aria-hidden="true"></i><?php }
							else {?>
								<i class="fa fa-check-circle colr" aria-hidden="true"></i><?php }?><?= $brokersData['third']['customersSupports']['through_branches']; ?></li>
						</ul>
					</li> <!-- .product -->
					<?php } ?> <!-- .product -->

				</ul> <!-- .cd-products-columns -->
			</div> <!-- .cd-products-wrapper -->
			
			</div> <!-- .cd-products-table -->
	</section> <!-- .cd-products-comparison-table -->
	<?php }else{ ?>
	<section class="cd-products-comparison-table container">
		<div class="container">
		<ul>
			<li>
				<h3 style="text-align: center;font-size: 25px;">Select at least two brokers.</h3>
			</li>
		</ul>

		</div>
	</section>
	<?php }?>