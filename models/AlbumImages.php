<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "album_images".
 *
 * @property integer $id
 * @property integer $album_id
 * @property integer $image_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUserAlbum $album
 * @property MubUserImages $image
 */
class AlbumImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'album_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'image_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserAlbum::className(), 'targetAttribute' => ['album_id' => 'id']],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserImages::className(), 'targetAttribute' => ['image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'album_id' => Yii::t('app', 'Album ID'),
            'image_id' => Yii::t('app', 'Image ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(MubUserAlbum::className(), ['id' => 'album_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(MubUserImages::className(), ['id' => 'image_id'])->where(['del_status' => '0']);
    }
}
